<?php

/**
 * 
 */
class AdminModel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function proses_login($where){
		return $this->db->get_where('tb_akses', $where);
	}

	function getAkses(){
		return $this->db->get('tb_akses');
	}

	function simpan_akses($where){
		return $this->db->insert('tb_akses', $where);
	}

	function getbyakses($id){
		$this->db->where('id_akses', $id);
		return $this->db->get('tb_akses');
	}
	function edit_akses($id, $where){
		$this->db->where('id_akses', $id);
		return $this->db->update('tb_akses', $where);
	}

	function delete_akses($id_akses){
		$this->db->where('id_akses', $id_akses);
		return $this->db->delete('tb_akses');
	}

	function getAgen(){
		return $this->db->get('tb_agen');
	}

	function getbyAgen($id){
		$this->db->where('id_agen', $id);
		return $this->db->get('tb_agen');
	}

	function simpan_agen($where){
		return $this->db->insert('tb_agen', $where);
	}

	function edit_agen($id, $where){
		$this->db->where('id_agen', $id);
		return $this->db->update('tb_agen', $where);
	}

	function delete_agen($id){
		$this->db->where('id_agen', $id);
		return $this->db->delete('tb_agen');
	}

	function getCustomer(){
		return $this->db->get('tb_customer');
	}

	function getbyCustomer($id){
		$this->db->where('id_customer', $id);
		return $this->db->get('tb_customer');
	}

	function simpan_customer($where){
		return $this->db->insert('tb_customer', $where);
	}

	function edit_customer($id, $where){
		$this->db->where('id_customer', $id);
		return $this->db->update('tb_customer', $where);
	}

	function delete_customer($id){
		$this->db->where('id_customer', $id);
		return $this->db->delete('tb_customer');
	}

	function getShipment(){
		return $this->db->get('tb_shipment');
	}

	function getbyShipment($id){
		$this->db->where('id_shipment', $id);
		return $this->db->get('tb_shipment');
	}

	function simpan_shipment($where){
		return $this->db->insert('tb_shipment', $where);
	}

	function edit_shipment($id, $where){
		$this->db->where('id_shipment', $id);
		return $this->db->update('tb_shipment', $where);
	}

	function delete_shipment($id){
		$this->db->where('id_shipment', $id);
		return $this->db->delete('tb_shipment');
	}

	function getJobs(){
		return $this->db->get("tb_jobs");
	}

	function simpan_jobs($where){
		return $this->db->insert('tb_jobs', $where);
	}

	function getbyJobs($id){
		return $this->db->query("SELECT * FROM tb_jobs INNER JOIN tb_customer ON tb_jobs.id_customer = tb_customer.id_customer WHERE tb_jobs.id_jobs = '$id'");
	}

	function getDataCustomer($id){
		return $this->db->query("SELECT * FROM tb_jobs INNER JOIN tb_customer ON tb_jobs.id_customer = tb_customer.id_customer WHERE tb_jobs.id_jobs = '$id'");
	}

	function edit_jobs($id, $where){
		$this->db->where('id_jobs', $id);
		return $this->db->update('tb_jobs', $where);
	}

	function getdatadesjob($id){
		$this->db->where('no_job', $id);
		return $this->db->get('tb_des_job');
	}
	
	function getdatadesjobinv($id){
		$this->db->where('no_job', $id);
		return $this->db->get('tb_des_inv');
	}

	function simpan_des_jobs($where){
		return $this->db->insert('tb_des_job', $where);
	}

	function edit_des_inv($id, $where)
	{
		$this->db->where('id_des_inv', $id);
		return $this->db->update('tb_des_inv', $where);
	}

	function hapus_des_jobs($id){
		$this->db->where('id_des_job', $id);
		return $this->db->delete('tb_des_job');
	}

	function delete_des_jobs($id){
		$this->db->where('no_job', $id);
		return $this->db->delete('tb_des_job');
	}

	function delete_jobs($id){
		$this->db->where('id_jobs', $id);
		return $this->db->delete('tb_jobs');
	}

	function getInvoice(){
		return $this->db->query("SELECT * FROM tb_invoice INNER JOIN tb_jobs ON tb_invoice.no_jobs = tb_jobs.no_job");
	}

	function getbyInvoice($id){
		$this->db->where('id_invoice', $id);
		return $this->db->get('tb_invoice');
	}

	function getDataInvoice($id){
		return $this->db->query("SELECT * FROM tb_invoice 
			INNER JOIN tb_jobs ON tb_invoice.no_jobs = tb_jobs.no_job 
			LEFT JOIN tb_customer ON tb_customer.id_customer = tb_jobs.id_customer 
			WHERE tb_invoice.id_invoice = '$id'");
	}

	function search_jobs($job){
		
		return $this->db->query("SELECT * FROM tb_jobs INNER JOIN tb_customer ON tb_jobs.id_customer = tb_customer.id_customer WHERE tb_jobs.no_job = '$job'");
	}

	function simpan_invoice($where){
		return $this->db->insert('tb_invoice', $where);
	}

	function simpan_invoice_kredit($data){
		return $this->db->insert('tb_hutang', $data);
	}

	function edit_invoice($id, $where){
		$this->db->where('id_invoice', $id);
		return $this->db->update('tb_invoice', $where);
	}

	function insert_piutang($id, $data){
		$get = $this->db->get_where('tb_piutang', ['id_invoice' => $id]);
		if($get->num_rows() > 0){
			$this->db->where('id_invoice', $id);
			return $this->db->update('tb_piutang', $data);
		}
		else{
			return $this->db->insert('tb_piutang', $data);
		}
	}

	function delete_invoice($id){
		$this->db->where('id_invoice', $id);
		return $this->db->delete('tb_invoice');
	}

	function simpan_des_invoice($where){
		return $this->db->insert('tb_des_inv', $where);
	}

	function getdatadesinvoice($id){
		$this->db->where('no_invo', $id);
		return $this->db->get('tb_des_inv');
	}

	function hapus_des_inv($id){
		$this->db->where('id_des_inv', $id);
		return $this->db->delete('tb_des_inv');
	}

	function delete_des_inv($id){
		$this->db->where('no_invo', $id);
		return $this->db->delete('tb_des_inv');
	}

	function getHutang(){
		return $this->db->get('tb_hutang');
	}

	function getbyHutang($id){
		$this->db->where('id_hutang', $id);
		return $this->db->get('tb_hutang');
	}

	function simpan_hutang($where){
		return $this->db->insert('tb_hutang', $where);
	}

	function edit_hutang($id, $where){
		$this->db->where('id_hutang', $id);
		return $this->db->update('tb_hutang', $where);
	}

	function hapus_hutang($id){
		$this->db->where('id_hutang', $id);
		return $this->db->delete('tb_hutang');
	}

	function getPiutang(){
		return $this->db->get('tb_piutang');
	}

	function getbyPiutang($id){
		$this->db->where('id_piutang', $id);
		return $this->db->get('tb_piutang');
	}

	function simpan_piutang($where){
		return $this->db->insert('tb_piutang', $where);
	}

	function edit_piutang($id, $where){
		$this->db->where('id_piutang', $id);
		return $this->db->update('tb_piutang', $where);
	}

	function hapus_piutang($id){
		$this->db->where('id_piutang', $id);
		return $this->db->delete('tb_piutang');
	}

	function bayar_piutang($id){
		$piutang = ['status' => "Lunas"];
		$this->db->where('id_piutang', $id);
		return $this->db->update('tb_piutang', $piutang);
	}

	function getBank(){
		return $this->db->get('tb_bank');
	}

	function getbyBank($id){
		$this->db->where('id_bank', $id);
		return $this->db->get('tb_bank');
	}

	function simpan_bank($where){
		return $this->db->insert('tb_bank', $where);
	}

	function edit_bank($id, $where){
		$this->db->where('id_bank', $id);
		return $this->db->update('tb_bank', $where);
	}

	function hapus_bank($id){
		$this->db->where('id_bank', $id);
		return $this->db->delete('tb_bank');
	}

	function getKasMasuk(){
		return $this->db->query("SELECT * FROM tb_kas_masuk INNER JOIN tb_bank ON tb_kas_masuk.id_bank = tb_bank.id_bank");
	}

	function simpan_kasmasuk($where){
		return $this->db->insert('tb_kas_masuk', $where);
	}

	function getBykMasuk($id){
		$this->db->where('id_kas_masuk', $id);
		return $this->db->get('tb_kas_masuk');
	}

	function edit_kasmasuk($id, $where){
		$this->db->where('id_kas_masuk', $id);
		return $this->db->update('tb_kas_masuk', $where);
	}

	function hapus_kas_masuk($id){
		$this->db->where('id_kas_masuk', $id);
		return $this->db->delete('tb_kas_masuk');
	}

	function getKasKeluar(){
		return $this->db->query("SELECT * FROM tb_kas_keluar INNER JOIN tb_bank ON tb_kas_keluar.id_bank = tb_bank.id_bank");
	}

	function simpan_kaskeluar($where){
		return $this->db->insert('tb_kas_keluar', $where);
	}

	function getBykKeluar($id){
		$this->db->where('id_kas_keluar', $id);
		return $this->db->get('tb_kas_keluar');
	}

	function edit_kaskeluar($id, $where){
		$this->db->where('id_kas_keluar', $id);
		return $this->db->update('tb_kas_keluar', $where);
	}

	function hapus_kas_keluar($id){
		$this->db->where('id_kas_keluar', $id);
		return $this->db->delete('tb_kas_keluar');
	}

	function getKasDebet(){
		return $this->db->query("SELECT * FROM tb_kas_debet INNER JOIN tb_bank ON tb_kas_debet.id_bank = tb_bank.id_bank");
	}

	function simpan_kasdebet($where){
		return $this->db->insert('tb_kas_debet', $where);
	}

	function getBykDebet($id){
		$this->db->where('id_kas_debet', $id);
		return $this->db->get('tb_kas_debet');
	}

	function edit_kasdebet($id, $where){
		$this->db->where('id_kas_debet', $id);
		return $this->db->update('tb_kas_debet', $where);
	}

	function hapus_kas_debet($id){
		$this->db->where('id_kas_debet', $id);
		return $this->db->delete('tb_kas_debet');
	}

	function getInvoiceByKredit(){
		return $this->db->query("SELECT * FROM tb_invoice LEFT JOIN tb_jobs ON tb_invoice.no_jobs = tb_jobs.no_job WHERE tb_invoice.type_pembayaran = 'Kredit'");
	}

	function edit_des_job($id, $data){
		$this->db->where('id_des_job', $id);
		return $this->db->update('tb_des_job', $data);
	}

	function edit_des_op($id, $data){
		$this->db->where('id_job_operational', $id);
		return $this->db->update('tb_job_opertaional', $data);
	}
}

?>
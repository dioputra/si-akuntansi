<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	function autentication_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$array = array(
					'username' => $username,
					'password' => $password
				);

		$proses = $this->AdminModel->proses_login($array);
		if ($proses->num_rows() > 0) {
			$data = array(
						"title" => "Success",
						"type" => "success",
						"text" => "Anda berhasil login..!",
						"link" => 'UserController/page_home/dashboard'
					);
			$this->load->view('alert/alert_success', $data);
		}else{
			$data = array(
						"title" => "Failed",
						"type" => "error",
						"text" => "Data yang anda masukkan tidak terdaftar..!",
						"link" => 'Welcome'
					);
			$this->load->view('alert/alert_success', $data);
		}
	}

	function logout(){
		$data = array(
						"title" => "Success",
						"type" => "success",
						"text" => "Berhasil Logout!",
						"link" => 'Welcome'
					);
			$this->load->view('alert/alert_success', $data);
	}
}

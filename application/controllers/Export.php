<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//load Spout Library
require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
//lets Use the Spout Namespaces
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Export extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

	}

	public function aruskasmasuk(){
        $header = ['No', 'Tgl', 'Deskripsi','Nama Bank', 'No. Rekening', 'Nominal'];
        $load_data = $this->AdminModel->getKasMasuk()->result();
        $no = 1;
        $my_data = [];
        $total= 0;
        foreach ($load_data as $key) {
            $total+= $key->nominal ;
        	$my_data[] = [$no, $key->tgl_transaksi, $key->deskripsi, $key->nama_bank, $key->no_rekening, $key->nominal];
        	$no++;
        }
        $my_data[$no]=[null, null,null, null, null, null];
        $my_data[$no+1]=[null, 'Total',null, null, null, "Rp ".number_format($total,2,',','.')];

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser('Arus Kas Masuk '.date('Y-m-d').'.xlsx');
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();

        // write ke Sheet pertama
        $writer->getCurrentSheet()->setName('Arus Kas Masuk');
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($my_data);
        $writer->close();
	}

	public function aruskaskeluar(){

        $header = ['No', 'Tgl', 'Deskripsi','Nama Bank', 'No. Rekening', 'Nominal'];
        $load_data = $this->AdminModel->getKasKeluar()->result();
        $no = 1;
        $my_data = [];
        $total= 0;
        foreach ($load_data as $key) {
            $total+= $key->nominal ;
        	$my_data[] = [$no, $key->tgl_transaksi, $key->deskripsi, $key->nama_bank, $key->no_rekening, $key->nominal];
        	$no++;
        }
        $my_data[$no]=[null, null,null, null, null, null];
        $my_data[$no+1]=[null, 'Total',null, null, null, "Rp ".number_format($total,2,',','.')];

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser('Arus Kas Keluar '.date('Y-m-d').'.xlsx');
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();


        $writer->getCurrentSheet()->setName('Arus Kas Keluar');
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($my_data);

        $writer->close();
	}

    public function laporanhutang(){
        // echo "string";die;
        $header = ['No', 'Tanggal Awal', 'Tanggal Jatuh Tempo','Nominal', 'Klien', 'Deskripsi'];
        $load_data = $this->AdminModel->getHutang()->result();
        $no = 1;
        $my_data = [];
        $total= 0;
        foreach ($load_data as $key) {
            $total += $key->nominal;
            $my_data[] = [$no, $key->tanggal_awal, $key->tgl_jatuh_tempo, number_format($key->nominal,2,',','.'), $key->klien, $key->deskripsi];
            $no++;
        }
        $my_data[$no]=[null, null,null, null, null, null];
        $my_data[$no+1]=[null, 'Total',null, "Rp ".number_format($total,2,',','.'), null, null];

        
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser('Laporan Hutang '.date('Y-m-d').'.xlsx');
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();


        $writer->getCurrentSheet()->setName('Laporan Hutang');
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($my_data);
        $writer->close();
    }

    public function laporanpiutang(){
        $header = ['No', 'Tanggal Awal', 'Tanggal Jatuh Tempo','Nominal', 'Klien', 'Deskripsi'];
        $load_data = $this->AdminModel->getPiutang()->result();
        $no = 1;
        $my_data = [];
        foreach ($load_data as $key) {
            $total += $key->nominal;
            $my_data[] = [$no, $key->tgl_awal, $key->tgl_jatuh_tempo, number_format($key->nominal,2,',','.'), $key->klien, $key->deskripsi];
            $no++;
        }
        $my_data[$no]=[null, null,null, null, null, null];
        $my_data[$no+1]=[null, 'Total',null, "Rp ".number_format($total,2,',','.'), null, null];

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser('Laporan Piutang '.date('Y-m-d').'.xlsx');

        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();

        $writer->getCurrentSheet()->setName('Laporan Piutang');
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($my_data);


        $writer->close();
    }

    public function laporanpembayaran(){
		// Setup Excel Header masing-masing Sheets
        $header = ['No', 'Invoice', 'Tgl','Job Number', 'Customer', 'No.MBL', 'Vessel & Voy', 'Condition', 'Amount'];
        $load_data = $this->AdminModel->getInvoiceByKredit()->result();
        $no = 1;
        $my_data = [];
        foreach ($load_data as $key) {
        	$my_data[] = [$no, $key->no_invoice, $key->date, $key->no_job, $key->customer, $key->mbl_no, $key->vessel_voy, $key->type_pembayaran];
        	$no++;
        }


        /*$data = [
            ['Agus', 'Jl. Jaksa Jakarta', 90],
            ['Budy', 'Jl. Palmerah Jakarta', 70],
            ['Cecep', 'Jl. Sukacita Bandung', 77],
        ];*/

        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('Laporan Pembayaran '.date('Y-m-d').'.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
               ->setFontBold()
               ->build();


        // write ke Sheet pertama
        $writer->getCurrentSheet()->setName('Laporan Pembayaran');
        // header Sheet pertama
        $writer->addRowWithStyle($header, $headerStyle);
        // data Sheet pertama
        $writer->addRows($my_data);

        // close writter
        $writer->close();
	}

}

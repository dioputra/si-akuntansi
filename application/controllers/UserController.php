<?php

/**
 * 
 */
class UserController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$file = $this->uri->segment(3);
		if (empty($file)) {
			$file = "dashboard";
		}
		$tanggal = date('Y-m-d');
			$bulan_now = date('Y-m');
		$data['jobs'] = $this->AdminModel->getJobs()->result();
			$data['total_jobs'] = $this->db->query("SELECT * FROM tb_jobs WHERE date_create = '$tanggal'")->num_rows();
			$data['total_invoice'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date = '$tanggal'")->num_rows();
			$data['total_transaksi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
			$data['total_labarugi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
		$data['file'] = $file;
		$this->load->view('home', $data);
	}

	function page_home(){
		$file = $this->uri->segment(3);
		$data['file'] = $file;

		if (empty($file)) {
			$tanggal = date('Y-m-d');
			$bulan_now = date('Y-m');
			$data['jobs'] = $this->AdminModel->getJobs()->result();
			$data['total_jobs'] = $this->db->query("SELECT * FROM tb_jobs WHERE date_create = '$tanggal'")->num_rows();
			$data['total_invoice'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date = '$tanggal'")->num_rows();
			$data['total_transaksi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
			$data['total_labarugi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
			$data['file'] = "dashboard";

		}elseif ($file == "dashboard") {
			$tanggal = date('Y-m-d');
			$bulan_now = date('Y-m');
			$data['jobs'] = $this->AdminModel->getJobs()->result();
			$data['total_jobs'] = $this->db->query("SELECT * FROM tb_jobs WHERE date_create = '$tanggal'")->num_rows();
			$data['total_invoice'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date = '$tanggal'")->num_rows();
			$data['total_transaksi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
			$data['total_labarugi'] = $this->db->query("SELECT * FROM tb_invoice WHERE create_date LIKE '$bulan_now%' ")->num_rows();
			$data['file'] = $file;
		}else if ($file == "hak_akses") {
			$data['akses'] = $this->AdminModel->getAkses()->result();
		}else if ($file == "form_hak_akses") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"username" => "",
							"password" => "",
							"nama" => "",
							"level" => "",
							"file" => "form_hak_akses",
							"btn" => "Simpan"
						);
			}else{
				$dataakses = $this->AdminModel->getbyakses($id)->row();
				$jumlah = $this->AdminModel->getbyakses($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"username" => $dataakses->username,
							"password" => $dataakses->password,
							"nama" => $dataakses->nama,
							"level" => $dataakses->level,
							"file" => "form_hak_akses",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"username" => "",
							"password" => "",
							"nama" => "",
							"level" => "",
							"file" => "form_hak_akses",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_agen") {
			$data['agen'] = $this->AdminModel->getAgen()->result();
		}else if ($file == "form_agen") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama" => "",
							"alamat" => "",
							"telp" => "",
							"email" => "",
							"file" => "form_agen",
							"btn" => "Simpan"
						);
			}else{
				$dataagen = $this->AdminModel->getbyAgen($id)->row();
				$jumlah = $this->AdminModel->getbyAgen($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"nama" => $dataagen->nama,
							"alamat" => $dataagen->alamat,
							"telp" => $dataagen->telp,
							"email" => $dataagen->email,
							"file" => "form_agen",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama" => "",
							"alamat" => "",
							"telp" => "",
							"email" => "",
							"file" => "form_agen",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_customer") {
			$data['customer'] = $this->AdminModel->getCustomer()->result();
		}else if ($file == "form_customer") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama" => "",
							"alamat" => "",
							"telp" => "",
							"email" => "",
							"file" => "form_customer",
							"btn" => "Simpan"
						);
			}else{
				$datacustomer = $this->AdminModel->getbyCustomer($id)->row();
				$jumlah = $this->AdminModel->getbyCustomer($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"nama" => $datacustomer->nama,
							"alamat" => $datacustomer->alamat,
							"telp" => $datacustomer->telp,
							"email" => $datacustomer->email,
							"file" => "form_customer",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama" => "",
							"alamat" => "",
							"telp" => "",
							"email" => "",
							"file" => "form_customer",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "master_shipment") {
			$data['shipment'] = $this->AdminModel->getShipment()->result();
		}else if ($file == "form_shipment") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"jenis_shipment" => "",
							"file" => "form_shipment",
							"btn" => "Simpan"
						);
			}else{
				$datashipment = $this->AdminModel->getbyShipment($id)->row();
				$jumlah = $this->AdminModel->getbyShipment($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"jenis_shipment" => $datashipment->jenis_shipment,
							"file" => "form_shipment",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"jenis_shipment" => "",
							"file" => "form_shipment",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "master_jobs") {
			$data['jobs'] = $this->AdminModel->getJobs()->result();
		}else if ($file == "form_jobs") {
			// $data['desjobs'] = $this->AdminModel->getdatadesjob($no_jobs)->result();
			$id = $this->uri->segment(4);
			$bulan = date('m');
			$tahun = date('Y');
			$jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun' ORDER BY id_jobs DESC")->row();
				$jumlah_jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun'")->num_rows();

				if ($jumlah_jobs > 0) {
					$list = $jobs->id_list + 1;
					$no_jobs = "JOBS/".$list."/".date('d/m/Y');
					
				}else{
					$no_jobs = "JOBS/1/".date('d/m/Y');
					$list = '1';
				}
			if (empty($id)) {
				$this->AdminModel->delete_des_jobs($no_jobs);
				$this->db->query("DELETE FROM `tb_job_opertaional` WHERE no_job = '$no_jobs'");
				$data = array(
							"tanggal" => date('d-m-Y'),
							"jobs_number" => $no_jobs,
							"id_list" => $list,
							"shipment" => $this->AdminModel->getShipment()->result(),
							"customer" => $this->AdminModel->getCustomer()->result(),
							"v_shipment" =>  "",
							"v_customer" =>  "",
							"no_bl" => "",
							"pol" => "",
							"vessel" => "",
							"destination" => "",
							"commodity" => "",
							"cntr_quantity" => "",
							"etd" => "",
							"date" => "",
							"refund_shipper" => "",
							"voyage" => "",
							"renmarsk" => "",
							"size" => "",
							"advance" => "",
							"note" => "",
							"datadesjobs" => $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '1'")->result(),
							"dataoperational" => $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '1'")->result(),
							"file" => "form_jobs",
							"action" => "UserController/simpan_jobs",
							"btn" => "Simpan"
						);
			}else{
				$datajobs = $this->AdminModel->getbyJobs($id)->row();
				$jumlah = $this->AdminModel->getbyJobs($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"tanggal" => $datajobs->tanggal,
							"jobs_number" => $datajobs->no_job,
							"shipment" => $this->AdminModel->getShipment()->result(),
							"customer" => $this->AdminModel->getCustomer()->result(),
							"no_bl" => $datajobs->no_bl,
							"v_shipment" =>  $datajobs->id_shipment,
							"v_customer" =>  $datajobs->id_customer,
							"pol" =>  $datajobs->p_o_l,
							"vessel" =>  $datajobs->vessel,
							"destination" =>  $datajobs->destination,
							"commodity" =>  $datajobs->commodity,
							"cntr_quantity" =>  $datajobs->cntr_quantity,
							"size" => $datajobs->size,
							"etd" =>  $datajobs->etd,
							"date" =>  $datajobs->date,
							"refund_shipper" => $datajobs->refund_shipper,
							"voyage" => $datajobs->voyage,
							"renmarsk" => $datajobs->renmarsk,
							"advance" => $datajobs->advance_received_on,
							"note" => $datajobs->note,
							"datadesjobs" => $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$datajobs->no_job'")->result(),
							"action" => "UserController/edit_jobs",
							"file" => "form_jobs",
							"btn" => "Edit"
						);
				}else{
					$this->AdminModel->delete_des_jobs($no_jobs);
					$data = array(
							"tanggal" => date('d-m-Y'),
							"jobs_number" => $no_jobs,
							"shipment" => $this->AdminModel->getShipment()->result(),
							"v_shipment" =>  "",
							"customer" =>  "",
							"no_bl" => "",
							"pol" => "",
							"vessel" => "",
							"destination" => "",
							"commodity" => "",
							"cntr_quantity" => "",
							"size" => "",
							"etd" => "",
							"date" => "",
							"refund_shipper" => "",
							"voyage" => "",
							"renmarsk" => "",
							"datadesjobs" => "",
							"advance" => "",
							"note" => "",
							"file" => "form_jobs",
							"action" => "UserController/simpan_jobs",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "edit_jobs") {
			$id = $this->uri->segment(4);
			$datajobs = $this->AdminModel->getbyJobs($id)->row();
			$jumlah = $this->AdminModel->getbyJobs($id)->num_rows();

			if ($jumlah > 0) {
				$data = array(	
						"tanggal" => $datajobs->tanggal,
						"jobs_number" => $datajobs->no_job,
						"shipment" => $this->AdminModel->getShipment()->result(),
						"customer" => $this->AdminModel->getCustomer()->result(),
						"no_bl" => $datajobs->no_bl,
						"v_shipment" =>  $datajobs->id_shipment,
						"v_customer" =>  $datajobs->id_customer,
						"pol" =>  $datajobs->p_o_l,
						"vessel" =>  $datajobs->vessel,
						"destination" =>  $datajobs->destination,
						"commodity" =>  $datajobs->commodity,
						"cntr_quantity" =>  $datajobs->cntr_quantity,
						"size" => $datajobs->size,
						"etd" =>  $datajobs->etd,
						"date" =>  $datajobs->date,
						"refund_shipper" => $datajobs->refund_shipper,
						"voyage" => $datajobs->voyage,
						"renmarsk" => $datajobs->renmarsk,
						"advance" => $datajobs->advance_received_on,
						"note" => $datajobs->note,
						"datadesjobs" => $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$datajobs->no_job'")->result(),
						"dataoperational" => $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$datajobs->no_job'")->result(),
						"action" => "UserController/edit_jobs",
						"file" => "edit_jobs",
						"btn" => "Edit"
					);
			}else{
				$data = array(
						"title" => "Warning!!",
						"type" => "warning",
						"text" => "Data yang anda cari tidak ada..!",
						"file" => "edit_jobs",
						"link" => 'UserController/page_home/master_jobs'
					);
			$this->load->view('alert/alert_success', $data);
			}
		}else if ($file == "data_invoice") {
			$data['invoice'] = $this->AdminModel->getInvoice()->result();
		}else if ($file == "form_invoice") {
			$id = $this->uri->segment(5);
			$id_jobs = $this->uri->segment(4);
			$datajobs = $this->AdminModel->getbyJobs($id_jobs)->row();
			$bulan = date('m');
			$tahun = date('Y');
			$invoice = $this->db->query("SELECT * FROM tb_invoice where month(create_date)='$bulan' AND year(create_date) = '$tahun' ORDER BY id_invoice DESC")->row();
				$jumlah = $this->db->query("SELECT * FROM tb_invoice where month(create_date)='$bulan' AND year(create_date) = '$tahun' ORDER BY id_invoice DESC")->num_rows();

				if ($jumlah > 0) {
					$list = $invoice->no_list + 1;
					$invoice_number = "AJI/INV/".$list."/".date('d/m/Y');
					
				}else{
					$invoice_number = "AJI/INV/1/".date('d/m/Y');
					$list = '1';
				}
			if (empty($id)) {
				$this->AdminModel->delete_des_inv($invoice_number);
				$data = array(
							"invoice_number" => $invoice_number,
							"tanggal" => "",
							"no_list" => $list,
							"jobs" => $datajobs->no_job,
							"customer" =>  $datajobs->nama,
							"no_bl" => $datajobs->no_bl,
							"qty" => $datajobs->cntr_quantity,
							"size" => $datajobs->size,
							"vessel" => $datajobs->vessel,
							"destination" => $datajobs->destination,
							"commodity" => $datajobs->commodity,
							"etd" => $datajobs->etd,
							"remarks" => $datajobs->renmarsk,
							"pembayaran" => "",
							"datadesinve" => $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '#'")->result(),
							"file" => "form_invoice",
							"action" => "UserController/simpan_invoice",
							"btn" => "Simpan"
						);
			}else{
				$datainvoice = $this->AdminModel->getbyInvoice($id)->row();
				$jumlah = $this->AdminModel->getbyInvoice($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"tanggal" => $datainvoice->date,
							"invoice_number" => $datainvoice->no_invoice,
							"no_list" => $datainvoice->no_list,
							"jobs" => $datajobs->no_job,
							"no_bl" => $datainvoice->no_bl,
							"v_jobs" =>  $datainvoice->no_jobs,
							"customer" =>  $datainvoice->customer,
							"qty" =>  $datainvoice->qty,
							"size" =>  $datainvoice->size,
							"vessel" =>  $datainvoice->vessel_voy,
							"destination" =>  $datainvoice->pod,
							"commodity" =>  $datainvoice->commodity,
							"pembayaran" => $datainvoice->pembayaran,
							"etd" =>  $datainvoice->etd,
							"remarks" =>  $datainvoice->renmarsk,
							"datadesinve" => $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '$datainvoice->no_invoice'")->result(),
							"action" => "UserController/edit_invoice",
							"file" => "form_invoice",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"invoice_number" => $invoice_number,
							"tanggal" => "",
							"no_list" => $list,
							"jobs" => $datajobs->no_job,
							"customer" =>  $datajobs->id_shipment,
							"no_bl" => "",
							"qty" => "",
							"size" => "",
							"vessel" => $datajobs->vessel,
							"destination" => $datajobs->destination,
							"commodity" => $datajobs->commodity,
							"etd" => $datajobs->etd,
							"remarks" => $datajobs->remarks,
							"datadesinve" => $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '#'")->result(),
							"file" => "form_invoice",
							"action" => "UserController/simpan_invoice",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "edit_invoice") {
			$id = $this->uri->segment(4);
			
				$datainvoice = $this->AdminModel->getbyInvoice($id)->row();
				$jumlah = $this->AdminModel->getbyInvoice($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"tanggal" => $datainvoice->date,
							"invoice_number" => $datainvoice->no_invoice,
							"no_list" => $datainvoice->no_list,
							"jobs" => $datainvoice->no_jobs,
							"mbl_no" => $datainvoice->mbl_no,
							"v_jobs" =>  $datainvoice->no_jobs,
							"customer" =>  $datainvoice->customer,
							"qty" =>  $datainvoice->qty,
							"size" =>  $datainvoice->size,
							"vessel" =>  $datainvoice->vessel_voy,
							"destination" =>  $datainvoice->pod,
							"commodity" =>  $datainvoice->commodity,
							"pembayaran" => $datainvoice->type_pembayaran,
							"etd" =>  $datainvoice->etd,
							"remarks" =>  $datainvoice->remarks,
							"datadesinve" => $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '$datainvoice->no_invoice'")->result(),
							"action" => "UserController/edit_invoice",
							"file" => "edit_invoice",
							"btn" => "Edit"
						);
				}else{
					$data = array(
						"title" => "Warning!!",
						"type" => "warning",
						"text" => "Data yang anda cari tidak ada..!",
						"file" => "edit_jobs",
						"link" => 'UserController/page_home/data_invoice'
					);
					$this->load->view('alert/alert_success', $data);
				}
		}else if ($file == "data_hutang") {
			$data['hutang'] = $this->AdminModel->getHutang()->result();
		}else if ($file == "form_hutang") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"tgl_awal" => "",
							"tgl_jatuh_tempo" => "",
							"nominal" => "",
							"deskripsi" => "",
							"klien" => "",
							"file" => "form_hutang",
							"btn" => "Simpan"
						);
			}else{
				$datahutang = $this->AdminModel->getbyHutang($id)->row();
				$jumlah = $this->AdminModel->getbyHutang($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"tgl_awal" => $datahutang->tanggal_awal,
							"tgl_jatuh_tempo" => $datahutang->tgl_jatuh_tempo,
							"nominal" => str_replace(".","",$datahutang->nominal),
							"deskripsi" => $datahutang->deskripsi,
							"klien" => $datahutang->klien,
							"file" => "form_hutang",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"tgl_awal" => "",
							"tgl_jatuh_tempo" => "",
							"nominal" => "",
							"deskripsi" => "",
							"klien" => "",
							"file" => "form_hutang",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_piutang") {
			$data['piutang'] = $this->AdminModel->getPiutang()->result();
		}else if ($file == "form_piutang") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"tgl_awal" => "",
							"tgl_jatuh_tempo" => "",
							"nominal" => "",
							"deskripsi" => "",
							"klien" => "",
							"file" => "form_piutang",
							"btn" => "Simpan"
						);
			}else{
				$datahutang = $this->AdminModel->getbyPiutang($id)->row();
				$jumlah = $this->AdminModel->getbyPiutang($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"tgl_awal" => $datahutang->tgl_awal,
							"tgl_jatuh_tempo" => $datahutang->tgl_jatuh_tempo,
							"nominal" => str_replace(".","",$datahutang->nominal),
							"deskripsi" => $datahutang->deskripsi,
							"klien" => $datahutang->klien,
							"file" => "form_piutang",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"tgl_awal" => "",
							"tgl_jatuh_tempo" => "",
							"nominal" => "",
							"deskripsi" => "",
							"klien" => "",
							"file" => "form_piutang",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_bank") {
			$data['bank'] = $this->AdminModel->getBank()->result();
		}else if ($file == "form_bank") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama" => "",
							"no_rekening" => "",
							"periode_awal" => "",
							"periode_akhir" => "",
							"jumlah_saldo" => "",
							"file" => "form_bank",
							"btn" => "Simpan"
						);
			}else{
				$databank = $this->AdminModel->getbyBank($id)->row();
				$jumlah = $this->AdminModel->getbyBank($id)->num_rows();

				if ($jumlah > 0) {
					$data = array(
							"nama" => $databank->nama_bank,
							"no_rekening" => $databank->no_rekening,
							"periode_awal" => $databank->periode_awal,
							"periode_akhir" => $databank->periode_akhir,
							"jumlah_saldo" => $databank->jumlah_saldo,
							"file" => "form_bank",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama" => "",
							"no_rekening" => "",
							"periode_awal" => "",
							"periode_akhir" => "",
							"jumlah_saldo" => "",
							"file" => "form_bank",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_kasmasuk") {
			$data['kmasuk'] = $this->AdminModel->getKasMasuk()->result();
		}else if ($file == "form_kas_masuk") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => "",
							"tgl" => "",
							"nominal" => "",
							"no_rekening" => "",
							"deskripsi" => "",
							"file" => "form_kas_masuk",
							"btn" => "Simpan"
						);
			}else{
				$datakasmasuk = $this->AdminModel->getBykMasuk($id)->row();
				$jumlah = $this->AdminModel->getBykMasuk($id)->num_rows();

				$databank = $this->AdminModel->getbyBank($datakasmasuk->id_bank)->row();
				if ($jumlah > 0) {
					$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => $datakasmasuk->id_bank,
							"tgl" => $datakasmasuk->tgl_transaksi,
							"no_rekening" => $databank->no_rekening,
							"nominal" => $datakasmasuk->nominal,
							"deskripsi" => $datakasmasuk->deskripsi,
							"file" => "form_kas_masuk",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama_bank" => "",
							"no_rekening" => "",
							"tgl" => "",
							"nominal" => "",
							"deskripsi" => "",
							"file" => "form_kas_masuk",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_kaskeluar") {
			$data['kkeluar'] = $this->AdminModel->getKasKeluar()->result();
		}else if ($file == "form_kas_keluar") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => "",
							"tgl" => "",
							"nominal" => "",
							"no_rekening" => "",
							"deskripsi" => "",
							"file" => "form_kas_keluar",
							"btn" => "Simpan"
						);
			}else{
				$datakaskeluar = $this->AdminModel->getBykKeluar($id)->row();
				$jumlah = $this->AdminModel->getBykKeluar($id)->num_rows();

				
				if ($jumlah > 0) {
					$databank = $this->AdminModel->getbyBank($datakaskeluar->id_bank)->row();
					$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => $datakaskeluar->id_bank,
							"tgl" => $datakaskeluar->tgl_transaksi,
							"no_rekening" => $databank->no_rekening,
							"nominal" => $datakaskeluar->nominal,
							"deskripsi" => $datakaskeluar->deskripsi,
							"file" => "form_kas_keluar",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => "",
							"tgl" => "",
							"nominal" => "",
							"no_rekening" => "",
							"deskripsi" => "",
							"file" => "form_kas_keluar",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "data_kasdebet") {
			$data['kdebet'] = $this->AdminModel->getKasDebet()->result();
		}else if ($file == "form_kas_debet") {
			$id = $this->uri->segment(4);
			if (empty($id)) {
				$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => "",
							"tgl" => "",
							"nominal" => "",
							"no_rekening" => "",
							"deskripsi" => "",
							"file" => "form_kas_debet",
							"btn" => "Simpan"
						);
			}else{
				$datakasdebet = $this->AdminModel->getBykDebet($id)->row();
				$jumlah = $this->AdminModel->getBykDebet($id)->num_rows();

				
				if ($jumlah > 0) {
					$databank = $this->AdminModel->getbyBank($datakasdebet->id_bank)->row();
					$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => $datakasdebet->id_bank,
							"tgl" => $datakasdebet->tgl_transaksi,
							"no_rekening" => $databank->no_rekening,
							"nominal" => $datakasdebet->nominal,
							"deskripsi" => $datakasdebet->deskripsi,
							"file" => "form_kas_debet",
							"btn" => "Edit"
						);
				}else{
					$data = array(
							"nama_bank" => $this->AdminModel->getBank()->result(),
							"v_bank" => "",
							"tgl" => "",
							"nominal" => "",
							"no_rekening" => "",
							"deskripsi" => "",
							"file" => "form_kas_debet",
							"btn" => "Simpan"
						);
				}
				
			}
		}else if ($file == "laporan_pendapatan") {
			$data['invoice'] = $this->AdminModel->getInvoice()->result();
		}else if ($file == "laporan_pembayaran") {
			$data['invoice'] = $this->AdminModel->getInvoiceByKredit()->result();
		}else if ($file == "laba_rugi") {
			$tgl_awal = $this->input->post('tgl_awal');
			$tgl_akhir = $this->input->post('tgl_akhir');
			if (empty($tgl_awal) && empty($tgl_akhir)) {
				$data = array(
							"total_debet" => "0",
							"total_kredit" => "0",
							"profit" => "0",
							"kas_keluar" => "0",
							"laba" => "0",
							"rugi" => "0",
							"file" => "laba_rugi"
						);
			}else{
				$total_debet = 0;
				$total_kredit = 0;
				$kas_keluar = 0;
				$datajobs = $this->db->query("SELECT * FROM tb_jobs WHERE date_create BETWEEN '$tgl_awal' AND '$tgl_akhir'")->result();

				foreach ($datajobs as $key) {
					$datadesjob = $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$key->no_job'")->result();
					foreach ($datadesjob as  $value) {
						$total_debet += str_replace(".","",$value->debit);
						$total_kredit += str_replace(".","",$value->kredit);
					}
						
				}
				$profit = $total_debet - $total_kredit;
				$datakaskeluar = $this->db->query("SELECT * FROM tb_kas_keluar WHERE create_date BETWEEN '$tgl_awal' AND '$tgl_akhir'")->result();
				foreach ($datakaskeluar as $keluar) {
					$kas_keluar += str_replace(".","",$keluar->nominal);
				}
				$laba_rugi = $profit - $kas_keluar;	
				if ($laba_rugi > 0) {
					$laba = $laba_rugi;
					$rugi = "0";
				}else{
					$laba = "0";
					$rugi = $laba_rugi;
				}
				$data = array(
							"total_debet" => number_format($total_debet),
							"total_kredit" => number_format($total_kredit),
							"profit" => number_format($profit),
							"kas_keluar" => number_format($kas_keluar),
							"laba" => number_format($laba),
							"rugi" => number_format($rugi),
							"file" => "laba_rugi"
						);
			}
		}else if ($file == "laporan_kas_masuk") {
			$data['kmasuk'] = $this->AdminModel->getKasMasuk()->result();
		}else if ($file == "laporan_kas_keluar") {
			$data['kkeluar'] = $this->AdminModel->getKasKeluar()->result();
		}else if ($file == "laporan_kas_debet") {
			$data['kdebet'] = $this->AdminModel->getKasDebet()->result();
		}else if ($file == "laporan_hutang") {
			$data['hutang'] = $this->AdminModel->getHutang()->result();
		}else if ($file == "laporan_piutang") {
			$data['piutang'] = $this->AdminModel->getPiutang()->result();
		}

		$this->load->view('home', $data);
	}

	function simpan_akses(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$level = $this->input->post('level');

		$arraydata = array(
						"username" => $username,
						"password" => $password,
						"nama" => $nama,
						"level" => $level
					);

		$proses = $this->AdminModel->simpan_akses($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_akses(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$level = $this->input->post('level');
		$id_akses = $this->input->post('id_akses');

		$arraydata = array(
						"username" => $username,
						"password" => $password,
						"nama" => $nama,
						"level" => $level
					);

		$proses = $this->AdminModel->edit_akses($id_akses, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_akses(){
		$id_akses = $this->input->post('id_akses');
		$proses = $this->AdminModel->delete_akses($id_akses);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_agen(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');

		$arraydata = array(
						"nama" => $nama,
						"alamat" => $alamat,
						"telp" => $telp,
						"email" => $email,
						"date_create" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_agen($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_agen(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$id_agen = $this->input->post('id_agen');

		$arraydata = array(
						"nama" => $nama,
						"alamat" => $alamat,
						"telp" => $telp,
						"email" => $email
					);

		$proses = $this->AdminModel->edit_agen($id_agen, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_agen(){
		$id_agen = $this->input->post('id_agen');
		$proses = $this->AdminModel->delete_agen($id_agen);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_customer(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');

		$arraydata = array(
						"nama" => $nama,
						"alamat" => $alamat,
						"telp" => $telp,
						"email" => $email,
						"date_create" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_customer($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_customer(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$id_customer = $this->input->post('id_customer');

		$arraydata = array(
						"nama" => $nama,
						"alamat" => $alamat,
						"telp" => $telp,
						"email" => $email
					);

		$proses = $this->AdminModel->edit_customer($id_customer, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_customer(){
		$id_customer = $this->input->post('id_customer');
		$proses = $this->AdminModel->delete_customer($id_customer);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_shipment(){
		$jenis_shipment = $this->input->post('jenis_shipment');

		$arraydata = array(
						"jenis_shipment" => $jenis_shipment,
						"date_create" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_shipment($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_shipment(){
		$jenis_shipment = $this->input->post('jenis_shipment');
		$id_shipment = $this->input->post('id_shipment');

		$arraydata = array(
						"jenis_shipment" => $jenis_shipment
					);

		$proses = $this->AdminModel->edit_shipment($id_shipment, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_shipment(){
		$id_shipment = $this->input->post('id_shipment');
		$proses = $this->AdminModel->delete_shipment($id_shipment);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function loaddatadesjob(){
		$bulan = date('m');
		$tahun = date('Y');
		$jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun' ORDER BY id_jobs DESC")->row();
			$jumlah_jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun'")->num_rows();

			if ($jumlah_jobs > 0) {
				$list = $jobs->id_list + 1;
				$no_jobs = "JOBS/".$list."/".date('d/m/Y');
				
			}else{
				$no_jobs = "JOBS/1/".date('d/m/Y');
				$list = '1';
			}
		$data['desjobs'] = $this->AdminModel->getdatadesjob($no_jobs)->result();
		$this->load->view('file/loaddatadesjob', $data);
	}
	function loaddatadesjobinv(){
		$bulan = date('m');
		$tahun = date('Y');
		$no_jobs = $this->input->post('jobs');

		$cek = $this->AdminModel->getdatadesjobinv($no_jobs)->num_rows();
		if($cek > 0){
			$data['desjobs'] = $this->AdminModel->getdatadesjob($no_jobs)->result();
			$data['desinv'] = $this->AdminModel->getdatadesjobinv($no_jobs)->result();
		}
		else{
			$data['desjobs'] = $this->AdminModel->getdatadesjob($no_jobs)->result();
		}
		$this->load->view('file/loaddatainv', $data);
	}

	function loaddatadesjoboper(){
		$bulan = date('m');
		$tahun = date('Y');
		$jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun' ORDER BY id_jobs DESC")->row();
			$jumlah_jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun'")->num_rows();

			if ($jumlah_jobs > 0) {
				$list = $jobs->id_list + 1;
				$no_jobs = "JOBS/".$list."/".date('d/m/Y');
				
			}else{
				$no_jobs = "JOBS/1/".date('d/m/Y');
				$list = '1';
			}
		$data['dataoperational'] = $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$no_jobs'")->result();
		$this->load->view('file/loaddataoper', $data);
	}

	function simpandesjob(){
		$no_job = $this->input->post('no_jobs');
		$deskripsi = $this->input->post('deskripsi');
		$debit = $this->input->post('debit');
		$kredit = $this->input->post('kredit');

		$arraydes = array(
						"no_job" => $no_job,
						"deskripsi" => $deskripsi,
						"debit" => $debit,
						"kredit" => $kredit
					);

		$proses = $this->AdminModel->simpan_des_jobs($arraydes);
		if ($proses) {
			$datadesjos = $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$no_job'")->result();
			$total_debet = 0;
			$total_kredit = 0;
			foreach ($datadesjos as $key) {
				$total_debet += str_replace(".","",$key->debit);
				$total_kredit += str_replace(".","",$key->kredit);
			}

			$datadesoper = $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$no_job'")->result();
			$totaldebitdesoper = 0;
			$totalkreditdesoper = 0;
			foreach ($datadesoper as $value) {
				$totaldebitdesoper += str_replace(".","",$value->debit);
				$totalkreditdesoper += str_replace(".","",$value->kredit);
			}

			$totalalldebit = $total_debet + $totaldebitdesoper;
			$totalallkredit = $total_kredit + $totalkreditdesoper;

			$totalloss_profit = $totalalldebit - $totalallkredit;

			echo json_encode(array("alert" => 'success',"deskripsi" => $deskripsi,"total_debet"=> number_format($totalalldebit, 0,".","."),"loss_profit"=> number_format($totalloss_profit, 0,".","."),"total_kredit"=> number_format($totalallkredit, 0,".",".")));
		}
	}

	function simpan_joboper(){
		$no_job = $this->input->post('no_jobs');
		$deskripsi = $this->input->post('deskripsi');
		$debit = $this->input->post('debit');
		$kredit = $this->input->post('kredit');

		$arraydes = array(
						"no_job" => $no_job,
						"deskripsi" => $deskripsi,
						"debit" => $debit,
						"kredit" => $kredit
					);

		$proses = $this->db->insert('tb_job_opertaional',$arraydes);
		if ($proses) {
			$datadesjos = $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$no_job'")->result();
			$total_debet = 0;
			$total_kredit = 0;
			foreach ($datadesjos as $key) {
				$total_debet += str_replace(".","",$key->debit);
				$total_kredit += str_replace(".","",$key->kredit);
			}

			$datadesoper = $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$no_job'")->result();
			$totaldebitdesoper = 0;
			$totalkreditdesoper = 0;
			foreach ($datadesoper as $value) {
				$totaldebitdesoper += str_replace(".","",$value->debit);
				$totalkreditdesoper += str_replace(".","",$value->kredit);
			}

			$totalalldebit = $total_debet + $totaldebitdesoper;
			$totalallkredit = $total_kredit + $totalkreditdesoper;

			$totalloss_profit = $totalalldebit - $totalallkredit;
			echo json_encode(array("alert" => 'success', "deskripsi" => $deskripsi, "total_debet" => number_format($totalalldebit, 0,".","."),"loss_profit"=> number_format($totalloss_profit, 0,".","."),"total_kredit"=> number_format($totalallkredit, 0,".",".")));
		}
	}

	function balance(){
		$kredit = $this->input->post('total_kredit');
		$advance = $this->input->post('advance');

		$balance = str_replace(".","",$advance) -  str_replace(".","",$kredit);
		echo json_encode(array("alert" => 'success',"balance"=> number_format($balance, 0,".",".")));
	}

	function hapus_des_jobs(){
		$id_des = $this->input->post('id_des');
		$proses = $this->AdminModel->hapus_des_jobs($id_des);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}
	}

	function cetak_jobs(){
		$id_jobs = $this->uri->segment(3);
		$data['data_jobs'] = $this->AdminModel->getDataCustomer($id_jobs)->row_array();
		$this->load->view('content_file/cetak_jobs', $data);
	}

	function cetak_invoice(){
		$id_invoice = $this->uri->segment(3);
		$data['data_inv'] = $this->AdminModel->getDataInvoice($id_invoice)->row_array();
		$this->load->view('content_file/cetak_invoice', $data);
	}

	function cetak_invoice_2(){
		$id_invoice = $this->uri->segment(3);
		$data['data_inv'] = $this->AdminModel->getDataInvoice($id_invoice)->row_array();
		$this->load->view('content_file/cetak_invoice2', $data);
	}

	function simpan_jobs(){
		$jobs_number = $this->input->post('jobs_number');
		$tanggal = date('d-m-Y');
		$shipment = $this->input->post('shipment');
		$customer = $this->input->post('customer');
		$no_bl = $this->input->post('no_bl');
		$vessel = $this->input->post('vessel');
		$destination = $this->input->post('destination');
		$commodity = $this->input->post('commodity');
		$cntr_quantity = $this->input->post('cntr_quantity');
		$size = $this->input->post('size');
		$pol = $this->input->post('pol');
		$etd = $this->input->post('etd');
		$date = $this->input->post('date');
		$refund_shipper = $this->input->post('refund_shipper');
		$voyage = $this->input->post('voyage');
		$renmarsk = $this->input->post('renmarsk');
		$advance = $this->input->post('advance');
		$note = $this->input->post('note');
		$bulan = date("m");
		$tahun = date("Y");
		$jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create)='$bulan' AND year(date_create) = '$tahun' ORDER BY id_jobs DESC")->row();
				$jumlah_jobs = $this->db->query("SELECT * FROM tb_jobs where month(date_create) ='$bulan' AND year(date_create) = '$tahun'")->num_rows();

				if ($jumlah_jobs > 0) {
					$list = $jobs->id_list + 1;
					
				}else{
					$list = '1';
				}

		// $datadesjobs = $this->db->query("SELECT * FROM tb_des_job where no_job = '$jobs_number'")->result();
		// foreach ($datadesjobs as $key) {
		// 	$deskripdes = $this->input->post('deskripdes'.$key->id_des_job);
		// 	$debitdes = $this->input->post('debitdes'.$key->id_des_job);
		// 	$kreditdes = $this->input->post('kreditdes'.$key->id_des_job);
		// 	$dataarray = array(
		// 					'deskripsi' => $deskripdes,
		// 					'debit' => $debitdes,
		// 					'kredit' => $kreditdes
		// 				);
		// 	$this->db->where('id_des_job', $key->id_des_job);
		// 	$this->db->update('tb_des_job', $dataarray);
		// }
		
		// $datadesoper = $this->db->query("SELECT * FROM tb_job_opertaional where no_job = '$jobs_number'")->result();
		// foreach ($datadesoper as $value) {
		// 	$desoperasional = $this->input->post('desoperasional'.$value->id_job_operational);
		// 	$debitoper = $this->input->post('debitoper'.$value->id_job_operational);
		// 	$kreditoper = $this->input->post('kreditoper'.$value->id_job_operational);
		// 	$arrayoper = array(
		// 					'deskripsi' => $desoperasional,
		// 					'debit' => $debitoper,
		// 					'kredit' => $kreditoper
		// 				);
		// 	$this->db->where('id_job_operational', $value->id_job_operational);
		// 	$this->db->update('tb_job_opertaional', $arrayoper);
		// }

		$arraydata = array(
						"id_shipment" => $shipment,
						"id_list" => $list,
						"no_job" => $jobs_number,
						"tanggal" => $tanggal,
						"id_customer" => $customer,
						"no_bl" => $no_bl,
						"p_o_l" => $pol,
						"vessel" => $vessel,
						"destination" => $destination,
						"commodity" => $commodity,
						"cntr_quantity" => $cntr_quantity,
						"size" => $size,
						"etd" => $etd,
						// "date" => $date,
						"refund_shipper" => $refund_shipper,
						"voyage" => $voyage,
						"renmarsk" => $renmarsk,
						"note" => $note,
						"advance_received_on" => $advance,
						"date_create" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_jobs($arraydata);
		if ($proses) {
			$data = array(
						"title" => "Success",
						"type" => "success",
						"text" => "Data berhasil tersimpan..!",
						"link" => 'UserController/page_home/master_jobs'
					);
			$this->load->view('alert/alert_success', $data);
		}else{
			$data = array(
						"title" => "failed",
						"type" => "error",
						"text" => "Data yang anda masukkan salam silahkan masukkan data yang lain..!",
						"link" => 'UserController/page_home/form_jobs'
					);
			$this->load->view('alert/alert_success', $data);
		}
	}

	function edit_jobs(){
		$jobs_number = $this->input->post('jobs_number');
		$shipment = $this->input->post('shipment');
		$customer = $this->input->post('customer');
		$no_bl = $this->input->post('no_bl');
		$vessel = $this->input->post('vessel');
		$destination = $this->input->post('destination');
		$commodity = $this->input->post('commodity');
		$cntr_quantity = $this->input->post('cntr_quantity');
		$size = $this->input->post('size');
		$pol = $this->input->post('pol');
		$etd = $this->input->post('etd');
		$date = $this->input->post('date');
		$refund_shipper = $this->input->post('refund_shipper');
		$voyage = $this->input->post('voyage');
		$renmarsk = $this->input->post('renmarsk');
		$id_jobs = $this->input->post('id_jobs');
		$advance = $this->input->post('advance');
		$note = $this->input->post('note');
		
		/*$datadesjobs = $this->db->query("SELECT * FROM tb_des_job where no_job = '$jobs_number'")->result();
		foreach ($datadesjobs as $key) {
			$deskripdes = $this->input->post('deskripdes'.$key->id_des_job);
			$debitdes = $this->input->post('debitdes'.$key->id_des_job);
			$kreditdes = $this->input->post('kreditdes'.$key->id_des_job);
			$dataarray = array(
							'deskripsi' => $deskripdes,
							'debit' => $debitdes,
							'kredit' => $kreditdes
						);
			$this->db->where('id_des_job', $key->id_des_job);
			$this->db->update('tb_des_job', $dataarray);
		}

		$datadesoper = $this->db->query("SELECT * FROM tb_job_opertaional where no_job = '$jobs_number'")->result();
		foreach ($datadesoper as $value) {
			$desoperasional = $this->input->post('desoperasional'.$value->id_job_operational);
			$debitoper = $this->input->post('debitoper'.$value->id_job_operational);
			$kreditoper = $this->input->post('kreditoper'.$value->id_job_operational);
			$arrayoper = array(
							'deskripsi' => $desoperasional,
							'debit' => $debitoper,
							'kredit' => $kreditoper
						);
			$this->db->where('id_job_operational', $value->id_job_operational);
			$this->db->update('tb_job_opertaional', $arrayoper);
		}*/

		$arraydata = array(
						"id_shipment" => $shipment,
						"no_job" => $jobs_number,
						"id_customer" => $customer,
						"no_bl" => $no_bl,
						"p_o_l" => $pol,
						"vessel" => $vessel,
						"destination" => $destination,
						"commodity" => $commodity,
						"cntr_quantity" => $cntr_quantity,
						"size" => $size,
						"etd" => $etd,
						"note" => $note,
						"advance_received_on" => $advance,
						"date" => $date,
						"refund_shipper" => $refund_shipper,
						"voyage" => $voyage,
						"renmarsk" => $renmarsk
					);

		$proses = $this->AdminModel->edit_jobs($id_jobs, $arraydata);
		if ($proses) {
			$data = array(
						"title" => "Success",
						"type" => "success",
						"text" => "Data berhasil tersimpan..!",
						"link" => 'UserController/page_home/master_jobs'
					);
			$this->load->view('alert/alert_success', $data);
		}else{
			$data = array(
						"title" => "failed",
						"type" => "error",
						"text" => "Data yang anda masukkan salam silahkan masukkan data yang lain..!",
						"link" => 'UserController/page_home/form_jobs'
					);
			$this->load->view('alert/alert_success', $data);
		}
	}

	function hapus_jobs(){
		$id_jobs = $this->input->post('id_jobs');
		$proses = $this->AdminModel->delete_jobs($id_jobs);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_des_oper(){
		$id_jobs = $this->input->post('id_des');
		$proses = $this->db->query("DELETE FROM `tb_job_opertaional` WHERE id_job_operational = '$id_jobs'");
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function search_jobs(){
		$job = $this->input->post('jobs');

		$datajobs = $this->AdminModel->search_jobs($job)->row();

		echo json_encode(array("customer" => $datajobs->nama, "vessel" => $datajobs->vessel, "destination" => $datajobs->destination, "commodity" => $datajobs->commodity, "etd" => $datajobs->etd));
	}

	function simpan_invoice(){
		$bulan = date("m");
		$tahun = date("Y");
		$jobs = $this->db->query("SELECT * FROM tb_invoice where month(create_date)='$bulan' AND year(create_date) = '$tahun' ORDER BY id_invoice DESC")->row();
		$jumlah_jobs = $this->db->query("SELECT * FROM tb_invoice where month(create_date) ='$bulan' AND year(create_date) = '$tahun'")->num_rows();

		if ($jumlah_jobs > 0) {
			$list = $jobs->no_list + 1;
		
		}else{
			$list = '1';
		}
		$invoice_number = $this->input->post('invoice_number');
		$tanggal = $this->input->post('tanggal');
		$no_jobs = $this->input->post('jobs');
		$customer = $this->input->post('customer');
		$no_bl = $this->input->post('no_bl');
		$qty = $this->input->post('qty');
		$size = $this->input->post('size');
		$vessel = $this->input->post('vessel');
		$destination = $this->input->post('destination');
		$commodity = $this->input->post('commodity');
		$etd = $this->input->post('etd');
		$remarks = $this->input->post('remarks');
		$pembayaran = $this->input->post("pembayaran");
		$amount_inv = $this->input->post("amount_inv");


		$arraydata = array(
				"no_invoice" => $invoice_number,
				"no_list" => $list,
				"no_jobs" => $no_jobs,
				"date" => $tanggal,
				"customer" => $customer,
				"mbl_no" => $no_bl,
				"qty" => $qty,
				"size" => $size,
				"type_pembayaran" => $pembayaran,
				"vessel_voy" => $vessel,
				"pod" => $destination,
				"commodity" => $commodity,
				"etd" => $etd,
				"remarks" => $remarks,
				"create_date" => date('Y-m-d')
			);

		$arr_tb_des_inv = array(
				"no_invo" => $invoice_number,
				"no_job" => $no_jobs,
				"amount" => $amount_inv,
				"create_date" => date('Y-m-d')
			);



		if($pembayaran == 'Kredit'){
			$get_hutang = $this->db->get_where('tb_des_inv',['no_invo' => $invoice_number]);
			if($get_hutang->num_rows() > 0){
				foreach ($get_hutang->result() as $key => $v) {
					$datakredit = array(
						"tanggal_awal" => date('Y-m-d'),
						"tgl_jatuh_tempo" => date('Y-m-d'),
						"nominal" => str_replace(".","",$v->amount),
						"deskripsi" => $v->deskripsi,
						"klien" => $customer,
						"status" => 'hutang',
						"create_date" => date('Y-m-d')
					);
					$proses = $this->AdminModel->simpan_invoice_kredit($datakredit);
				}
			}
		}
		else{
			$proses = $this->AdminModel->simpan_invoice($arraydata);
			$des_inv = $this->db->insert('tb_des_inv', $arr_tb_des_inv);
		}

		if ($proses) {
			$data = array(
				"title" => "Success",
				"type" => "success",
				"text" => "Data berhasil tersimpan..!",
				"link" => 'UserController/page_home/data_invoice'
			);
			$this->load->view('alert/alert_success', $data);
		}else{
			$data = array(
				"title" => "failed",
				"type" => "error",
				"text" => "Data yang anda masukkan salam silahkan masukkan data yang lain..!",
				"link" => 'UserController/page_home/form_invoice'
			);
			$this->load->view('alert/alert_success', $data);
		}
		
	}

	function edit_invoice(){
		$amount = [];
		$invoice_number = $this->input->post('invoice_number');
		$tanggal = $this->input->post('tanggal');
		$no_jobs = $this->input->post('jobs');
		$customer = $this->input->post('customer');
		$mbl_no = $this->input->post('mbl_no');
		$qty = $this->input->post('qty');
		$size = $this->input->post('size');
		$vessel = $this->input->post('vessel');
		$destination = $this->input->post('destination');
		$commodity = $this->input->post('commodity');
		$etd = $this->input->post('etd');
		$remarks = $this->input->post('remarks');
		$pembayaran = $this->input->post("pembayaran");
		$id_invoice = $this->input->post('id_invoice');
		$kredit = $this->input->post('kredit');
		$amount [] = str_replace(".", "",$kredit);

		$datadesjobs = $this->db->query("SELECT * FROM tb_des_inv where no_invo = '$invoice_number'")->result();
		foreach ($datadesjobs as $key) {
			$deskripdes = $this->input->post('deskripsi'.$key->id_des_inv);
			$debitdes = $this->input->post('unit'.$key->id_des_inv);
			$kreditdes = $this->input->post('editmount'.$key->id_des_inv);
			$amount [] =  str_replace(".", "",$kreditdes);
			$dataarray = array(
							'deskripsi' => $deskripdes,
							'unit' => $debitdes,
							'amount' => $kreditdes
						);
			$this->db->where('id_des_inv', $key->id_des_inv);
			$this->db->update('tb_des_inv', $dataarray);
		}
		$arraydata = array(
						"no_jobs" => $no_jobs,
						"date" => $tanggal,
						"customer" => $customer,
						"mbl_no" => $mbl_no,
						"qty" => $qty,
						"size" => $size,
						"vessel_voy" => $vessel,
						"pod" => $destination,
						"commodity" => $commodity,
						"type_pembayaran" => $pembayaran,
						"etd" => $etd,
						"remarks" => $remarks,
						"create_date" => date('Y-m-d')
					);

		if($pembayaran == 'Kredit'){
			$piutang = array(
				"tgl_awal" => $tanggal,
				"tgl_jatuh_tempo" => $etd,
				"nominal" => array_sum($amount),
				"deskripsi" => $remarks,
				"klien" => $customer,
				"status" => 'Belum Lunas',
				"id_invoice" => $id_invoice,
				"create_Date" => date('Y-m-d')
			);
			$insert_piutang = $this->AdminModel->insert_piutang($id_invoice, $piutang);
		}
		
		$proses = $this->AdminModel->edit_invoice($id_invoice, $arraydata);
		if ($proses) {
			$data = array(
						"title" => "Success",
						"type" => "success",
						"text" => "Data berhasil tersimpan..!",
						"link" => 'UserController/page_home/data_invoice'
					);
			$this->load->view('alert/alert_success', $data);
		}else{
			$data = array(
						"title" => "failed",
						"type" => "error",
						"text" => "Data yang anda masukkan salam silahkan masukkan data yang lain..!",
						"link" => 'UserController/page_home/form_invoice'
					);
			$this->load->view('alert/alert_success', $data);
		}
	}

	function hapus_invoice(){
		$id = $this->input->post('id_invoice');
		$proses = $this->AdminModel->delete_invoice($id);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_des_invoice(){
		$deskripsi = $this->input->post('deskripsi');
		$unit = $this->input->post('unit');
		$amount = $this->input->post('amount');
		$no_invoice = $this->input->post('no_invoice');

		$dataarray = array(
						"no_invo" => $no_invoice,
						"deskripsi" => $deskripsi,
						"unit" => $unit,
						"amount" => $amount
					);

		$proses = $this->AdminModel->simpan_des_invoice($dataarray);
		if ($proses) {
			echo json_encode(array("alert" => "success"));
		}
	}

	function loaddatadesinvoice(){
		$bulan = date('m');
		$tahun = date('Y');
		$invoice = $this->db->query("SELECT * FROM tb_invoice where month(create_date)='$bulan' AND year(create_date) = '$tahun' ORDER BY id_invoice DESC")->row();
			$jumlah = $this->db->query("SELECT * FROM tb_invoice where month(create_date)='$bulan' AND year(create_date) = '$tahun' ORDER BY id_invoice DESC")->num_rows();

			if ($jumlah > 0) {
				$list = $invoice->no_list + 1;
				$invoice_number = "AJI/INV/".$list."/".date('d/m/Y');
				
			}else{
				$invoice_number = "AJI/INV/1/".date('d/m/Y');
				$list = '1';
			}
		$data['desinv'] = $this->AdminModel->getdatadesinvoice($invoice_number)->result();
		$this->load->view('file/loaddatadesinvoice', $data);
	}

	function hapus_des_inv(){
		$id_des = $this->input->post('id_des');
		$proses = $this->AdminModel->hapus_des_inv($id_des);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}
	}

	function simpan_hutang(){
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$nominal = $this->input->post('nominal');
		$deskripsi = $this->input->post('deskripsi');
		$klien = $this->input->post('klien');

		$arraydata = array(
						"tanggal_awal" => $tgl_awal,
						"tgl_jatuh_tempo" => $tgl_jatuh_tempo,
						"nominal" => $nominal,
						"deskripsi" => $deskripsi,
						"klien" => $klien,
						"status" => "Belum Dibayar",
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_hutang($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_hutang(){
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$nominal = $this->input->post('nominal');
		$deskripsi = $this->input->post('deskripsi');
		$klien = $this->input->post('klien');
		$id_hutang = $this->input->post('id_hutang');

		$arraydata = array(
						"tanggal_awal" => $tgl_awal,
						"tgl_jatuh_tempo" => $tgl_jatuh_tempo,
						"nominal" => $nominal,
						"deskripsi" => $deskripsi,
						"klien" => $klien
					);

		$proses = $this->AdminModel->edit_hutang($id_hutang, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_hutang(){
		$id_hutang = $this->input->post('id_hutang');
		$proses = $this->AdminModel->hapus_hutang($id_hutang);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_piutang(){
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$nominal = $this->input->post('nominal');
		$deskripsi = $this->input->post('deskripsi');
		$klien = $this->input->post('klien');

		$arraydata = array(
						"tgl_awal" => $tgl_awal,
						"tgl_jatuh_tempo" => $tgl_jatuh_tempo,
						"nominal" => $nominal,
						"deskripsi" => $deskripsi,
						"klien" => $klien,
						"status" => "Belum Dibayar",
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_piutang($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_piutang(){
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
		$nominal = $this->input->post('nominal');
		$deskripsi = $this->input->post('deskripsi');
		$klien = $this->input->post('klien');
		$id_piutang = $this->input->post('id_piutang');

		$arraydata = array(
						"tgl_awal" => $tgl_awal,
						"tgl_jatuh_tempo" => $tgl_jatuh_tempo,
						"nominal" => $nominal,
						"deskripsi" => $deskripsi,
						"klien" => $klien
					);

		$proses = $this->AdminModel->edit_piutang($id_piutang, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_piutang(){
		$id_piutang = $this->input->post('id_piutang');
		$proses = $this->AdminModel->hapus_piutang($id_piutang);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function bayar_piutang(){
		$id_piutang = $this->input->post('id_piutang');
		$proses = $this->AdminModel->bayar_piutang($id_piutang);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_bank(){
		$nama_bank = $this->input->post('nama_bank');
		$no_rekening = $this->input->post('no_rekening');
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$jumlah_saldo = $this->input->post('jumlah_saldo');

		$arraydata = array(
						"nama_bank" => $nama_bank,
						"no_rekening" => $no_rekening,
						"periode_awal" => $periode_awal,
						"periode_akhir" => $periode_akhir,
						"jumlah_saldo" => $jumlah_saldo,
						"sisa_saldo" => $jumlah_saldo,
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_bank($arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_bank(){
		$nama_bank = $this->input->post('nama_bank');
		$no_rekening = $this->input->post('no_rekening');
		$periode_awal = $this->input->post('periode_awal');
		$periode_akhir = $this->input->post('periode_akhir');
		$jumlah_saldo = $this->input->post('jumlah_saldo');
		$id_bank = $this->input->post('id_bank');

		$arraydata = array(
						"nama_bank" => $nama_bank,
						"no_rekening" => $no_rekening,
						"periode_awal" => $periode_awal,
						"periode_akhir" => $periode_akhir,
						"jumlah_saldo" => $jumlah_saldo
					);

		$proses = $this->AdminModel->edit_bank($id_bank, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_bank(){
		$id = $this->input->post('id_bank');
		$proses = $this->AdminModel->hapus_bank($id);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function search_bank(){
		$id = $this->input->post('id_bank');
		$databank = $this->AdminModel->getbyBank($id)->row();
		$jumlah = $this->AdminModel->getbyBank($id)->num_rows();
		if ($jumlah > 0) {
			echo json_encode(array("no_rekening" => $databank->no_rekening));
		}
	}

	function simpan_kasmasuk(){
		$id_bank = $this->input->post('nama_bank');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"id_bank" => $id_bank,
						"tgl_transaksi" => $tanggal,
						"nominal" => $nominal,
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_kasmasuk($arraydata);
		if ($proses) {
			$databank = $this->AdminModel->getbyBank($id_bank)->row();
			$sisa_saldo = str_replace(".","",$databank->sisa_saldo) + str_replace(".","",$nominal);
			$where = array(
						"sisa_saldo" => number_format($sisa_saldo,0,".",".")
					);
			$this->db->where('id_bank', $databank->id_bank);
			$this->db->update('tb_bank', $where);

			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}


	function edit_kasmasuk(){
		$id_bank = $this->input->post('nama_bank');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');
		$id_kas = $this->input->post('id_kas');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"id_bank" => $id_bank,
						"nominal" => $nominal
					);
		$datakas = $this->AdminModel->getBykMasuk($id_kas)->row();
		$nominal_akhir =str_replace(".","",$datakas->nominal) - str_replace(".","",$nominal);
		$databank = $this->AdminModel->getbyBank($id_bank)->row();
		$kurang_saldo = str_replace(".","",$databank->sisa_saldo) - $nominal_akhir;
		$arraybank = array(
							"sisa_saldo" => number_format($kurang_saldo,0,".",".")
						);
		$this->db->where('id_bank', $id_bank);
		$this->db->update('tb_bank', $arraybank);

		$proses = $this->AdminModel->edit_kasmasuk($id_kas, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_kas_masuk(){
		$id_kas = $this->input->post('id_kas');
		$proses = $this->AdminModel->hapus_kas_masuk($id_kas);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_kaskeluar(){
		$id_bank = $this->input->post('nama_bank');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"tgl_transaksi" => $tanggal,
						"id_bank" => $id_bank,
						"nominal" => $nominal,
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_kaskeluar($arraydata);
		if ($proses) {
			$databank = $this->AdminModel->getbyBank($id_bank)->row();
			$sisa_saldo = str_replace(".","",$databank->sisa_saldo) - str_replace(".","",$nominal);
			$where = array(
						"sisa_saldo" => number_format($sisa_saldo,0,".",".")
					);
			$this->db->where('id_bank', $databank->id_bank);
			$this->db->update('tb_bank', $where);

			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_kaskeluar(){
		$id_bank = $this->input->post('nama_bank');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');
		$id_kas = $this->input->post('id_kas');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"id_bank" => $id_bank,
						"tgl_transaksi" => $tanggal,
						"nominal" => $nominal
					);
		$datakas = $this->AdminModel->getBykKeluar($id_kas)->row();
		$nominal_akhir =str_replace(".","",$datakas->nominal) - str_replace(".","",$nominal);
		$databank = $this->AdminModel->getbyBank($id_bank)->row();
		$kurang_saldo = str_replace(".","",$databank->sisa_saldo) + $nominal_akhir;
		$arraybank = array(
							"sisa_saldo" => number_format($kurang_saldo,0,".",".")
						);
		$this->db->where('id_bank', $id_bank);
		$this->db->update('tb_bank', $arraybank);

		$proses = $this->AdminModel->edit_kaskeluar($id_kas, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_kas_keluar(){
		$id_kas = $this->input->post('id_kas');
		$proses = $this->AdminModel->hapus_kas_keluar($id_kas);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function simpan_kasdebet(){
		$id_bank = $this->input->post('nama_bank');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"tgl_transaksi" => $tanggal,
						"id_bank" => $id_bank,
						"nominal" => $nominal,
						"create_date" => date('Y-m-d')
					);

		$proses = $this->AdminModel->simpan_kasdebet($arraydata);
		if ($proses) {
			$databank = $this->AdminModel->getbyBank($id_bank)->row();
			$sisa_saldo = str_replace(".","",$databank->sisa_saldo) - str_replace(".","",$nominal);
			$where = array(
						"sisa_saldo" => number_format($sisa_saldo,0,".",".")
					);
			$this->db->where('id_bank', $databank->id_bank);
			$this->db->update('tb_bank', $where);

			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function edit_kasdebet(){
		$id_bank = $this->input->post('nama_bank');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$nominal = $this->input->post('nominal');
		$id_kas = $this->input->post('id_kas');

		$arraydata = array(
						"deskripsi" => $deskripsi,
						"id_bank" => $id_bank,
						"tgl_transaksi" => $tanggal,
						"nominal" => $nominal
					);
		$datakas = $this->AdminModel->getBykdebet($id_kas)->row();
		$nominal_akhir =str_replace(".","",$datakas->nominal) - str_replace(".","",$nominal);
		$databank = $this->AdminModel->getbyBank($id_bank)->row();
		$kurang_saldo = str_replace(".","",$databank->sisa_saldo) + $nominal_akhir;
		$arraybank = array(
							"sisa_saldo" => number_format($kurang_saldo,0,".",".")
						);
		$this->db->where('id_bank', $id_bank);
		$this->db->update('tb_bank', $arraybank);

		$proses = $this->AdminModel->edit_kasdebet($id_kas, $arraydata);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function hapus_kas_debet(){
		$id_kas = $this->input->post('id_kas');
		$proses = $this->AdminModel->hapus_kas_debet($id_kas);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	function export_aruskasmasuk(){
		// $data = $this->AdminModel->getKasMasuk()->result();
		// $id_bank = $this->input->post('nama_bank');
		// $tanggal = $this->input->post('tanggal');
		// $deskripsi = $this->input->post('deskripsi');
		// $nominal = $this->input->post('nominal');

		// $arraydata = array(
		// 	"deskripsi" => $deskripsi,
		// 	"id_bank" => $id_bank,
		// 	"tgl_transaksi" => $tanggal,
		// 	"nominal" => $nominal,
		// 	"create_date" => date('Y-m-d')
		// // );
  //         $semua_pengguna = $this->export_model->getAll()->result();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Nama')
                      ->setCellValue('C1', 'Jenis Kelamin')
                      ->setCellValue('D1', 'Tanggal Lahir')
                      ->setCellValue('E1', 'Umur');

          $kolom = 2;
          $nomor = 1;
          foreach($semua_pengguna as $pengguna) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $nomor)
                           ->setCellValue('B' . $kolom, '')
                           ->setCellValue('C' . $kolom, '')
                           ->setCellValue('D' . $kolom, '')
                           ->setCellValue('E' . $kolom, '');

               $kolom++;
               $nomor++;

          }

          $writer = new Xlsx($spreadsheet);

          header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="Latihan.xlsx"');
	  header('Cache-Control: max-age=0');

	  $writer->save('php://output');
	}

	public function deskripdes($id){
		$deskripdes = $this->input->post('deskripdes');
		$proses = $this->AdminModel->edit_des_job($id,['deskripsi' => $deskripdes]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}
	public function unitrate($id){
		$unitrate = $this->input->post('unitrate');
		$proses = $this->AdminModel->edit_des_job($id,['unitrate' => $unitrate]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function debitdes($id){
		$debitdes = $this->input->post('debitdes');
		$proses = $this->AdminModel->edit_des_job($id,['debit' => $debitdes]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function kreditdes($id){
		$kreditdes = $this->input->post('kreditdes');
		$proses = $this->AdminModel->edit_des_job($id,['kredit' => $kreditdes]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function desoperasional($id){
		$desoperasional = $this->input->post('desoperasional');
		$proses = $this->AdminModel->edit_des_op($id,['deskripsi' => $desoperasional]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function debitdesoper($id){
		$debitdesoper = $this->input->post('debitdesoper');
		$proses = $this->AdminModel->edit_des_op($id,['debit' => $debitdesoper]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function kreditdesoper($id){
		$kreditdesoper = $this->input->post('kreditdesoper');
		$proses = $this->AdminModel->edit_des_op($id,['kredit' => $kreditdesoper]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function getdesjob($id){
		$this->db->where('id_des_job', $id);
		$get = $this->db->get('tb_des_job');
		echo $get->row()->debit;
	}

	public function getdesop($id){
		$this->db->where('id_job_operational', $id);
		$get = $this->db->get('tb_job_opertaional');
		echo $get->row()->debit;
	}



	// Invoice
	public function deskripsi($id){
		$deskripsi = $this->input->post('deskripsi');
		$proses = $this->AdminModel->edit_des_inv($id,['deskripsi' => $deskripsi]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function unit($id){
		$unit = $this->input->post('unit');
		$proses = $this->AdminModel->edit_des_inv($id,['unit' => $unit]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

	public function amount($id){
		$amount = $this->input->post('amount');
		$proses = $this->AdminModel->edit_des_inv($id,['amount' => $amount]);
		if ($proses) {
			echo json_encode(array("alert" => 'success'));
		}else{
			echo json_encode(array("alert" => 'failed'));
		}
	}

		public function getdesinv($id){
		$this->db->where('id_des_inv', $id);
		$get = $this->db->get('tb_des_inv');
		echo $get->row()->amount;
	}

	public function get_amount(){
		$id = $this->input->post('jobs');
		$this->db->where('no_job', $id);
		$get = $this->db->get('tb_job_opertaional');
		if($get->num_rows() > 0){
			$amount_op = [];
			foreach ($get->result() as $key => $value) {
				$amount_op[] = $value->debit;
			}
		}else{
			$amount_op[] = 0;
		}

		$this->db->where('no_job', $id);
		$get_job = $this->db->get('tb_des_job');
		if($get_job->num_rows() > 0){
			$amount_des = [];
			foreach ($get_job->result() as $key => $value) {
				$amount_des[] = $value->debit;
			}
		}else{
			$amount_des[] = 0;
		}

		$tot_op = array_sum($amount_op);
		$tot_des = array_sum($amount_des);

		$tot_amount = $tot_op + $tot_des ;

		echo $tot_amount;
	}

	function loaddatadesjob_edit(){
		$jobs = $this->input->post('jobs');
		$data['desjobs'] =$this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$jobs'")->result();
		$this->load->view('file/loaddatadesjob', $data);
	}

	function loaddatadesjoboper_edit(){
		$jobs = $this->input->post('jobs');
		$data['dataoperational'] = $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$jobs'")->result();
		$this->load->view('file/loaddataoper', $data);
	}
}

?>
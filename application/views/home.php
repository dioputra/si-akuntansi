<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>SIA - Sistem Informasi Akutansi</title>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>assets/assets/images/favicon.ico" type="image/x-icon">
  <!-- Vector CSS -->
  <link href="<?php echo base_url();?>assets/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>assets/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/assets/css/app-style.css" rel="stylesheet"/>

  <!--Data Tables -->
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

  <!--Select Plugins-->
  <link href="<?php echo base_url();?>assets/assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
  <!--inputtags-->
  <link href="<?php echo base_url();?>assets/assets/plugins/inputtags/css/bootstrap-tagsinput.css" rel="stylesheet" />
  <!--multi select-->
  <link href="<?php echo base_url();?>assets/assets/plugins/jquery-multi-select/multi-select.css" rel="stylesheet" type="text/css">
  <!--Bootstrap Datepicker-->
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
  <!--Touchspin-->
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">
  
</head>

<body onload="loaddata()">

<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="index.html">
       <img src="<?php echo base_url();?>assets/assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
       <h5 class="logo-text">SIA-AKUTANSI</h5>
     </a>
	 </div>
	 <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">PILIHAN MENU</li>
      <li>
        <a href="<?php echo site_url('UserController/page_home/');?>" class="waves-effect">
          <i class="icon-home"></i> <span>Dashboard</span> 
        </a>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
  		  <li><a href="<?php echo site_url('UserController/page_home/hak_akses');?>"><i class="fa fa-circle-o"></i> User</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/data_agen');?>"><i class="fa fa-circle-o"></i> Agen</a></li>
  		  <li><a href="<?php echo site_url('UserController/page_home/data_customer');?>"><i class="fa fa-circle-o"></i> Customer</a></li>
        </ul>
      </li>
      <li>
        <a href="<?php echo site_url('UserController/page_home/master_shipment');?>" class="waves-effect">
          <i class="icon-rocket"></i> <span>Shipment</span>
        </a>
      </li>
	     <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-briefcase"></i>
          <span>Jobs</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/master_jobs');?>"><i class="fa fa-circle-o"></i> Master Jobs</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/data_invoice');?>"><i class="fa fa-circle-o"></i> Invoice</a></li>
        </ul>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Hutang Piutang</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/data_hutang');?>"><i class="fa fa-circle-o"></i> Hutang</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/data_piutang');?>"><i class="fa fa-circle-o"></i> Piutang</a></li>
        <!-- <li><a href="<?php echo site_url('UserController/page_home/data_customer');?>"><i class="fa fa-circle-o"></i> Hutang Customer</a></li> -->
        </ul>
      </li>
      <li>
        <a href="<?php echo site_url('UserController/page_home/data_bank');?>" class="waves-effect">
          <i class="icon-credit-card"></i> <span>Saldo Awal</span>
        </a>
      </li>
       <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Arus Kas</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/data_kasmasuk');?>"><i class="fa fa-circle-o"></i> Kas Masuk</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/data_kaskeluar');?>"><i class="fa fa-circle-o"></i> Kas Keluar</a></li>
        <!-- <li><a href="<?php echo site_url('UserController/page_home/data_kasdebet');?>"><i class="fa fa-circle-o"></i> Kas Debet</a></li>-->
        </ul> 
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/laporan_pendapatan');?>"><i class="fa fa-circle-o"></i> Pendapatan</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/laporan_pembayaran');?>"><i class="fa fa-circle-o"></i> Pembayaran</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/laba_rugi');?>"><i class="fa fa-circle-o"></i> Laba Rugi</a></li>
        </ul>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Laporan Kas</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/laporan_kas_masuk');?>"><i class="fa fa-circle-o"></i> Kas Masuk</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/laporan_kas_keluar');?>"><i class="fa fa-circle-o"></i> Kas Keluar</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/laporan_kas_debet');?>"><i class="fa fa-circle-o"></i> Kas Debet</a></li>
        </ul>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="icon-list"></i>
          <span>Laporan Hutang Piutang</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <li><a href="<?php echo site_url('UserController/page_home/laporan_hutang');?>"><i class="fa fa-circle-o"></i> Hutang</a></li>
        <li><a href="<?php echo site_url('UserController/page_home/laporan_piutang');?>"><i class="fa fa-circle-o"></i> Piutang</a></li>
        </ul>
      </li>
    </ul>
	 
   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top bg-white">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <li class="nav-item">
      <form class="search-bar">
        <input type="text" class="form-control" placeholder="Enter keywords">
         <a href="javascript:void();"><i class="icon-magnifier"></i></a>
      </form>
    </li>
  </ul>
  <ul class="navbar-nav align-items-center right-nav-link">
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile"><img src="<?php echo base_url();?>assets/assets/images/user.png" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="<?php echo base_url();?>assets/assets/images/user.png" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title">Administrator</h6>
            <p class="user-subtitle">Admin</p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><a href="<?php echo site_url('Welcome/logout');?>"><i class="icon-power mr-2"></i> Logout</a></li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->
	     <?php 
          include "content_file/".$file.".php";

       ?>

    </div>
    <!-- End container-fluid-->
    
  </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2019 PT.ASSOBA JAYA INTERNASIONAL
        </div>
      </div>
    </footer>
	<!--End footer-->
   
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- waves effect js -->
  <script src="<?php echo base_url();?>assets/assets/js/waves.js"></script>
  <!-- CK EDITOR -->
  <!-- sidebar-menu js -->
  <script src="<?php echo base_url();?>assets/assets/js/sidebar-menu.js"></script>
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/assets/js/app-script.js"></script>
  
  <!-- Vector map JavaScript -->
  <script src="<?php echo base_url();?>assets/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- Chart js -->
  <script src="<?php echo base_url();?>assets/assets/plugins/Chart.js/Chart.min.js"></script>


  <!--Data Tables js-->
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();
      
      } );

    </script>
  
  <!--Sweet Alerts -->
  <script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/sweet_alert.js"></script>
   <!--Select Plugins Js-->
    <script src="<?php echo base_url();?>assets/assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo base_url();?>assets/assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

    <!--Bootstrap Datepicker Js-->
    <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script>
      $('#default-datepicker').datepicker({
        todayHighlight: true
      });
      $('#etd').datepicker({
        todayHighlight: true
      });
      $('#periode_awal').datepicker({
        todayHighlight: true
      });
      $('#periode_akhir').datepicker({
        todayHighlight: true
      });

      $('#date').datepicker({
        todayHighlight: true
      });

      $('#tgl_awal').datepicker({
        todayHighlight: true
      });

      $('#tgl_jatuh_tempo').datepicker({
        todayHighlight: true
      });

      $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });

      $('#inline-datepicker').datepicker({
         todayHighlight: true
      });

      $('#dateragne-picker .input-daterange').datepicker({
       });

    </script>
    <!--Multi Select Js-->
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
    
    
    <script>
        $(document).ready(function() {
            $('.single-select').select2();
      
            $('.multiple-select').select2();

        //multiselect start

            $('#my_multi_select1').multiSelect();
            $('#my_multi_select2').multiSelect({
                selectableOptgroup: true
            });

            $('#my_multi_select3').multiSelect({
                selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                afterInit: function (ms) {
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function (e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function (e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });

         $('.custom-header').multiSelect({
              selectableHeader: "<div class='custom-header'>Selectable items</div>",
              selectionHeader: "<div class='custom-header'>Selection items</div>",
              selectableFooter: "<div class='custom-header'>Selectable footer</div>",
              selectionFooter: "<div class='custom-header'>Selection footer</div>"
            });



          });

    </script>

  <script src="<?php echo base_url();?>assets/assets/ckeditor/ckeditor.js"></script>
  <script>
      CKEDITOR.replace( 'editor1' );
  </script>
  <!-- Index js -->
  <script src="<?php echo base_url();?>assets/assets/js/index.js"></script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Success</title>
	 <!-- Vector CSS -->
  <link href="<?php echo base_url();?>assets/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>assets/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/assets/css/app-style.css" rel="stylesheet"/>

  <!--Data Tables -->
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<!--Sweet Alerts -->
  <script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
  <script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/sweet_alert.js"></script>
  			<script>
  				swal.fire({
                                    type: '<?php echo $type;?>',
                                    title: '<?php echo $title;?>',
                                    text: '<?php echo $text;?>'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url($link);?>";
                                });

                    </script>
</body>
</html>
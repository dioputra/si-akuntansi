<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>SIA - Sistem Informasi Akutansi</title>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>assets/assets/images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/assets/css/app-style.css" rel="stylesheet"/>
  
</head>

<body>
 <!-- Start wrapper-->
 <div id="wrapper">
	<div class="card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown">
		<div class="card-body">
		 <div class="card-content p-2">
		 	<div class="text-center">
		 		<img src="<?php echo base_url();?>assets/assets/images/logo-icon.png">
		 	</div>
		  <div class="card-title text-uppercase text-center py-3">Halaman Login</div>
		    <form action="<?php echo site_url('Welcome/autentication_login');?>" method="POST">
			  <div class="form-group">
			   <div class="position-relative has-icon-right">
				  <label for="exampleInputUsername" class="sr-only">Username</label>
				  <input type="text" id="exampleInputUsername" name="username" class="form-control form-control-rounded" placeholder="Masukkan Username">
				  <div class="form-control-position">
					  <i class="icon-user"></i>
				  </div>
			   </div>
			  </div>
			  <div class="form-group">
			   <div class="position-relative has-icon-right">
				  <label for="exampleInputPassword" class="sr-only">Password</label>
				  <input type="password" id="exampleInputPassword" name="password" class="form-control form-control-rounded" placeholder="Masukkan Password">
				  <div class="form-control-position">
					  <i class="icon-lock"></i>
				  </div>
			   </div>
			  </div>
			<div class="form-row mr-0 ml-0">
			 <div class="form-group col-6">
			   <div class="icheck-primary">
                <input type="checkbox" id="user-checkbox" checked="" />
                <!-- <label for="user-checkbox">Remember me</label> -->
			  </div>
			 </div>
			</div>
			 <button type="submit" class="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">Login</button>
			  <div class="text-center pt-3">
				
				<p class="text-muted"></p>
			  </div>
			 </form>
		   </div>
		  </div>
	     </div>
    
     <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	</div><!--wrapper-->
	
  <!-- Bootstrap core JavaScript-->
	<script src="<?php echo base_url();?>assets/assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/assets/js/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/assets/js/bootstrap.min.js"></script>


	<!-- simplebar js -->
	<script src="<?php echo base_url();?>assets/assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- waves effect js -->
 	<script src="<?php echo base_url();?>assets/assets/js/waves.js"></script>
	<!-- sidebar-menu js -->
	<script src="<?php echo base_url();?>assets/assets/js/sidebar-menu.js"></script>
  <!-- Custom scripts -->
	<script src="<?php echo base_url();?>assets/assets/js/app-script.js"></script>

	<!--Sweet Alerts -->
	<script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
	<script src="<?php echo base_url();?>assets/assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/assets/js/sweet_alert.js"></script>
	
</body>
</html>

      <?php
          foreach ($dataoperational as $oper) {
            
        ?>
       <tr>
          <td width="40%"><input type="text" onblur="desoperasional_(<?php echo $oper->id_job_operational;?>)" id="desoperasional<?php echo $oper->id_job_operational;?>" name="desoperasional<?php echo $oper->id_job_operational;?>" value="<?php echo $oper->deskripsi;?>" class="form-control"></td>
          
          <td width="20%"><input type="text" onblur="debitdesoper_(<?php echo $oper->id_job_operational;?>)" id="debitdesoper<?php echo $oper->id_job_operational;?>" name="debitoper<?php echo $oper->id_job_operational;?>" value="<?php echo $oper->debit;?>" class="debitoper<?php echo $oper->id_job_operational;?> form-control" ></td>
          <td width="20%"><input type="text" onblur="kreditdesoper_(<?php echo $oper->id_job_operational;?>)" id="kreditdesoper<?php echo $oper->id_job_operational;?>" name="kreditoper<?php echo $oper->id_job_operational;?>" value="<?php echo $oper->kredit;?>" class="kreditoper<?php echo $oper->id_job_operational;?> form-control" ></td>
          <td width="10%">
             <input type="checkbox" class="myCheck" id="cekoper<?php echo $oper->id_job_operational;?>" onclick="ceklisoper(<?php echo $oper->id_job_operational;?>)">
          </td>
          <td>
            <button type="button" onclick="hapusoper(<?php echo $oper->id_job_operational;?>)" class="btn btn-defult" title="hapus">-</button>
          </td>
        </tr>
        <script type="text/javascript">
               
          $(".debitoper<?php echo $oper->id_job_operational;?>").keyup(function() {
            var myval = this.value;
            var res = formatRupiahOper(myval, '');
            $(".debitoper<?php echo $oper->id_job_operational;?>").val(res);
          });

          $(".kreditoper<?php echo $oper->id_job_operational;?>").keyup(function() {
              var myval = this.value;
              var res = formatRupiahOper(myval, '');
              $(".kreditoper<?php echo $oper->id_job_operational;?>").val(res);
          });
        </script>
      <?php 
        }
       ?>

<script type="text/javascript">
    

        /* Fungsi formatRupiah */
        function formatRupiahOper(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }


        function ceklisoper(id_oper) {
    
            var checkBox = document.getElementById("cekoper"+id_oper);
            var renmarsk = $("#renmarsk").val().replace(/\./g , '');
            var debit = $("#debitdesoper"+id_oper).val().replace(/\./g , '');
            if(renmarsk == '' ){
                var remark = 0;
                var hasil = debit*remark;
                var url = "<?php echo base_url(); ?>index.php/UserController/getdesop/"+id_oper ;
                $.ajax({
                   type: "GET",
                   url: url,
                   success: function(val)
                   {
                        if($("#cekoper"+id_oper).is(':checked')){
                            $("#debitdesoper"+id_oper).val(convertToRupiah(hasil));
                        }else{
                           $("#debitdesoper"+id_oper).val(convertToRupiah(val));
                        }
                   }
                });
            }else{
                var remark = renmarsk;
                var hasil = debit*remark;
                var sebelumnya = debit/remark;

                if($("#cekoper"+id_oper).is(':checked')){
                    debitdesoper_(id_oper, hasil);
                    $("#debitdesoper"+id_oper).val(convertToRupiah(hasil));
                }else{
                    debitdesoper_(id_oper, sebelumnya);
                   $("#debitdesoper"+id_oper).val(convertToRupiah(sebelumnya));
                }
            }
        }
      </script>
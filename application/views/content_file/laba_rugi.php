<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Laporan Laba Rugi</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Laba Rugi</li>
         </ol>
       </div>

      <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('export/labarugi/');?>" class="btn btn-outline-primary"><i class="fa fa-table mr-1"></i>Export Excel</a>
          </div>
        </div>

     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Laba Rugi</div>
            <div class="card-body">
              <p>Pencarian berdasarkan tanggal</p>
              <form action="<?php echo site_url('UserController/page_home/laba_rugi');?>" method="post">
              <table>
                <tr>
                  <td><input type="text" name="tgl_awal" class="form-control"></td>
                  <td>S/D</td>
                  <td><input type="text" name="tgl_akhir" class="form-control"></td>
                  <td><input type="submit" name="tgl_awal" class="btn btn-info" value="cari"></td>
                </tr>
              </table>
              </form>
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                
                <tbody>
                    <tr>
                      <td>Total Kredit</td>
                      <td>Rp. <?php echo $total_kredit;?></td>
                    </tr>
                    <tr>
                      <td>Total Debet</td>
                      <td>Rp. <?php echo $total_debet;?></td>
                    </tr>
                    <tr>
                      <td>Profit</td>
                      <td>Rp. <?php echo $profit;?></td>
                    </tr>
                    <tr>
                      <td>Kas Keluar</td>
                      <td>Rp. <?php echo $kas_keluar;?></td>
                    </tr>
                    <tr>
                      <td>Laba</td>
                      <td>Rp. <?php echo $laba;?></td>
                    </tr>
                    <tr>
                      <td>Rugi</td>
                      <td>Rp. <?php echo $rugi;?></td>
                    </tr>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div>
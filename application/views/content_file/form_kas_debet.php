<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Form Input Kas Debet</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Kas Debet</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
     </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Tanggal Transaksi</label>
                    <div class="col-sm-9">
                    <input type="text" id="date" class="form-control" value="<?php echo $tgl;?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                    <input type="text" id="deskripsi" class="form-control" value="<?php echo $deskripsi;?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                      <select id="nama_bank" onclick="databank()" class="form-control" name="nama_bank">
                        <option value="">- PILIH -</option>
                        <?php
                          foreach ($nama_bank as $key) {
                        ?>
                        <option <?php if($v_bank == $key->id_bank) echo "selected";?> value="<?php echo $key->id_bank;?>"><?php echo $key->nama_bank;?></option>
                        <?php
                          }
                        ?>
                      </select>
                      <input type="hidden" id="id_kas_debet" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">No. Rekening</label>
                    <div class="col-sm-9">
                    <input type="text" id="no_rekening" value="<?php echo $no_rekening;?>" class="form-control" readonly="readonly">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Nominal</label>
                    <div class="col-sm-9">
                    <input type="text" id="nominal" class="form-control" value="<?php echo $nominal;?>" placeholder="0">
                    </div>
                  </div>
                  
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_akses()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/data_kasmasuk');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_akses(){
              var nama_bank = $("#nama_bank").val();
              var deskripsi = $("#deskripsi").val();
              var tanggal = $("#date").val();
              var nominal = $("#nominal").val();
              var id_kas = $("#id_kas_debet").val();

              if (nama_bank == "" ||deskripsi == "" || nominal == ""|| tanggal == "") {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Silahkan lengkapi data anda.....!'
                        });
              }else{

                if (id_kas == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_kasdebet');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama_bank" : nama_bank,
                        "tanggal" : tanggal,
                        "deskripsi" : deskripsi,
                        "nominal" : nominal
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_kasdebet');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                    "url" : "<?php echo site_url('UserController/edit_kasdebet');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama_bank" : nama_bank,
                        "tanggal" : tanggal,
                        "deskripsi" : deskripsi,
                        "nominal" : nominal,
                        "id_kas" : id_kas
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil diedit.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_kasdebet');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }
              }
          }

          function databank(){
            var nama_bank = $("#nama_bank").val();
            $.ajax({
                "url" : "<?php echo site_url('UserController/search_bank');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                      "id_bank" : nama_bank
                  },
                  success:function(data){
                      $("#no_rekening").val(data.no_rekening);
                  }
            });
          }

      </script>
      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('nominal');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>
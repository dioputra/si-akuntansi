<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Data Agen</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Agen</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_agen');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Agen</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Lengkap</th>
                        <th>Alamat</th>
                        <th>Telp</th>
                        <th>Email</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($agen as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->nama;?></td>
                        <td><?php echo $key->alamat;?></td>
                        <td><?php echo $key->telp;?></td>
                        <td><?php echo $key->email;?></td>
                        <td>
                            <a href="<?php echo site_url('UserController/page_home/form_agen/');?><?php echo $key->id_agen;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_agen;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Nama Lengkap</th>
                        <th>Alamat</th>
                        <th>Telp</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_agen){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_agen');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_agen" : id_agen
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/data_agen');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


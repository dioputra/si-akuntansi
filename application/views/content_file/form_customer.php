<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Customer</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Customer</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                    <input type="text" id="nama" value="<?php echo $nama;?>" class="form-control">
                    <input type="hidden" id="id_customer" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9">
                    <input type="text" id="alamat" value="<?php echo $alamat;?>" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Telp</label>
                    <div class="col-sm-9">
                    <input type="text" id="telp" class="form-control" value="<?php echo $telp;?>" placeholder="02311 xxxx">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                    <input type="email" id="email" class="form-control" value="<?php echo $email;?>" placeholder="exampler@test.com">
                    </div>
                  </div>
                  
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_akses()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/data_customer');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_akses(){
              var nama = $("#nama").val();
              var alamat = $("#alamat").val();
              var telp = $("#telp").val();
              var email = $("#email").val();
              var id_customer = $("#id_customer").val();

              if (nama == "" || alamat == "" ||telp == "" || email == "") {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Silahkan lengkapi data agen.....!'
                        });
              }else{

                if (id_customer == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_customer');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama" : nama,
                        "alamat" : alamat,
                        "telp" : telp,
                        "email" : email
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_customer');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                    "url" : "<?php echo site_url('UserController/edit_customer');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama" : nama,
                        "alamat" : alamat,
                        "telp" : telp,
                        "email" : email,
                        "id_customer" : id_customer
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil diedit.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_customer');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }
              }
          }

      </script>
      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('telp');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '' : '';
            rupiah += separator + ribuan.join('');
          }
     
          rupiah = split[1] != undefined ? rupiah + '' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? ' ' + rupiah : '');
        }
      </script>
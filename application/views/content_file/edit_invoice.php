<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Invoice</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Invoice</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                    <form action="<?php echo site_url($action);?>" method="POST" >
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Invoice Number</label>
                    <div class="col-sm-5">
                        <input type="text" id="invoice_number" name="invoice_number" value="<?php echo $invoice_number;?>" class="form-control" readonly="readonly">
                        <input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo $this->uri->segment(4);?>" class="form-control" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Tanggal</label>
                    <div class="col-sm-5">
                        <input type="text" id="date" name="tanggal" value="<?php echo $tanggal;?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Jobs</label>
                    <div class="col-sm-9">
                        <input type="text" id="jobs" name="jobs" value="<?php echo $jobs;?>" class="form-control" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Customer</label>
                    <div class="col-sm-9">
                        <input type="text" id="customer" name="customer" value="<?php echo $customer;?>" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">No. MBL</label>
                    <div class="col-sm-9">
                        <input type="text" id="no_bl" name="mbl_no" value="<?php echo $mbl_no;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">QTY</label>
                    <div class="col-sm-9">
                        <input type="text" id="pol" name="qty" value="<?php echo $qty;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Size</label>
                    <div class="col-sm-9">
                        <input type="text" id="pol" name="size" value="<?php echo $size;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">VESSEL & VOY</label>
                    <div class="col-sm-9">
                        <input type="text" id="vessel" name="vessel" value="<?php echo $vessel;?>" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Destination</label>
                    <div class="col-sm-9">
                        <input type="text" id="destination" name="destination" value="<?php echo $destination;?>" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Commodity</label>
                    <div class="col-sm-9">
                        <input type="text" id="commodity" name="commodity" value="<?php echo $commodity;?>" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">ETD</label>
                    <div class="col-sm-5">
                        <input type="text" id="etd2" name="etd" value="<?php echo $etd;?>" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Remarks</label>
                    <div class="col-sm-5">
                        <input type="text" id="remarks" name="remarks" value="<?php echo $remarks;?>" class="form-control" required="required" >
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Type Pembayaran</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="pembayaran" id="pembayaran">
                          <option <?php if($pembayaran == "Cash") echo "selected";?>>Cash</option>
                          <option <?php if($pembayaran == "Kredit") echo "selected";?>>Kredit</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                           <table class="table table-striped" id="Tdeskripsi">
                              <thead>
                                <tr>
                                  <th scope="col">DETAILS OF CHARGES</th>
                                  <th scope="col">UNIT RATE</th>
                                  <th scope="col">AMOUNT</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody id="loaddata">
                                
                              </tbody>
                              <tbody> 
                                <?php
                                  foreach ($datadesinve as $value) {
                                ?>
                               <tr>
                                  <td width="40%"><input type="text"  name="deskripsi<?php echo $value->id_des_inv;?>" value="<?php echo $value->deskripsi;?>" class="form-control"></td>
                                  <td width="25%"><input type="text"  name="unit<?php echo $value->id_des_inv;?>" value="<?php echo $value->unit;?>" class="form-control"></td>
                                  <td width="25%"><input type="text" id="amount" name="editmount<?php echo $value->id_des_inv;?>" value="<?php echo $value->amount;?>" class="form-control"></td>
                                  <td>
                                    
                                    <button type="button" onclick="hapus(<?php echo $value->id_des_inv;?>)" class="btn btn-defult" title="hapus">-</button></td>
                                </tr>
                              <?php } ?>
                                <tr>
                                  <td width="40%"><input type="text" id="deskripsi" name="deskripsi" class="form-control"></td>
                                  <td width="25%"><input type="text" id="unit" name="debit" class="form-control"></td>
                                  <td width="25%"><input type="text" id="amount" name="kredit" class="form-control"></td>
                                  <td><button type="button" class="btn btn-defult" onclick="simpan_des_invoice()">+</button></td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/data_invoice');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
                  </form>
              </div>
          </div>
        </div>
      </div><!--End Row-->
      <script type="text/javascript">
        
        function search_jobs(){
            var jobs = $("#jobs").val();

            $.ajax({
                "url" : "<?php echo site_url('UserController/search_jobs');?>",
                "type" : "POST",
                "dataType" : "json",
                "data" : {
                    "jobs" : jobs
                },
                success:function(data){
                   $("#customer").val(data.customer);
                   $("#vessel").val(data.vessel);
                   $("#destination").val(data.destination);
                   $("#commodity").val(data.commodity);
                   $("#etd2").val(data.etd);
                }
            });
        }

      </script>
      <script type="text/javascript">
          function loaddata(){
              $("#loaddata").load("<?php echo site_url('UserController/loaddatadesinvoice/');?>");
          }

          function simpan_des_invoice(){
              var deskripsi = $("#deskripsi").val();
              var unit = $("#unit").val();
              var amount = $("#amount").val();
              var no_invoice = $("#invoice_number").val();

              $.ajax({
                  "url" : "<?php echo site_url('UserController/simpan_des_invoice');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                      "no_invoice" : no_invoice,
                      "deskripsi" : deskripsi,
                      "amount" : amount,
                      "unit" : unit
                  },
                  success:function(data){
                     

                    if (data.alert == "success") {
                         document.location = "<?php echo site_url('UserController/page_home/edit_invoice');?>/<?php echo $this->uri->segment(4);?>";
                        $("#deskripsi").val("");
                        $("#unit").val("");
                        $("#amount").val("");
                    }
                      
                  }
              });
          }

          function hapus(id_des){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/hapus_des_inv');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                    "id_des" : id_des
                  },
                  success:function(hasil){
                      document.location = "<?php echo site_url('UserController/page_home/edit_invoice');?>/<?php echo $this->uri->segment(4);?>";
                  }
              });
          }
      </script>

      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('amount');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>
      <script type="text/javascript">
    
        var rupiah3 = document.getElementsByTagName('editmount');
        rupiah3.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah3.value = formatRupiah3(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah3(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>
<!DOCTYPE html>
<html>
<head>
	<title>Laporan Invoice</title>
	<style type="text/css">
		h3{
			margin-block-start: 0;
    		margin-block-end: 0;
			}

			.col-md-6{
				width: 50%;
				float: left;
			}
	</style>
</head>
<?php 
function TanggalIndo($date){
    $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);
 
    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;        
    return($result);
}
?>
<body style="font-family: segoe ui;font-size: 14px;">
	<center>
		<img src="<?php echo base_url();?>assets/assets/images/header-invoice.png" width="100%" height="150px">
		
		<br><br><h2>BILLING INVOICE</h2>
		<h2>AJI/INV1/1/1/1</h2>
	</center>
	<table style="border: 2px solid #000;width: 50%;float: left;padding: 0px 10px;">
		<tr style="background: #ddd;height: 40px;">
			<td style="text-align: center;">SHIPPER :</td>
		</tr>
		<tr>
			<td style="text-align: center;height: 70px;"><h3><?php echo $data_inv['customer'];?></h3></td>
		</tr>
	</table>
	<table style="border: 2px solid #000;width: 50%;float: left;padding:0px 10px;">
		<tr>
			<td>DATE</td>
			<td>:</td>
			<td><?php echo $data_inv['date'];?></td>
		</tr>
		<tr>
			<td>JOB</td>
			<td>:</td>
			<td><?php echo $data_inv['no_jobs'];?></td>
		</tr>
		<tr>
			<td>MBL NO</td>
			<td>:</td>
			<td><?php echo $data_inv['mbl_no'];?></td>
		</tr>
		<tr>
			<td>QTY</td>
			<td>:</td>
			<td><?php echo $data_inv['qty'];?></td>
		</tr>
		<tr>
			<td>SIZE</td>
			<td>:</td>
			<td><?php echo $data_inv['size'];?></td>
		</tr>
	</table>
	<div style="width: 97%;border: 2px solid #000;padding: 10px;float: left;">
		<table width="100%">
			<tr>
				<td width="40%">VESSEL & VOY</td>
				<td width="5%">:</td>
				<td><?php echo $data_inv['vessel_voy'];?></td>
			</tr>
			<tr>
				<td>ETD</td>
				<td>:</td>
				<td><?php echo $data_inv['etd'];?></td>
			</tr>
			<tr>
				<td>POL</td>
				<td>:</td>
				<td><?php echo $data_inv['p_o_l'];?></td>
			</tr>
			<tr>
				<td>POD</td>
				<td>:</td>
				<td><?php echo $data_inv['pod'];?></td>
			</tr>
			<tr>
				<td>COMMODITY</td>
				<td>:</td>
				<td><?php echo $data_inv['commodity'];?></td>
			</tr>
		</table>
	<table width="100%" style="border-top: 2px solid #000;">
			
		<tr>
			<td><strong>REMARKS</strong><span style="padding-left: 20px;">$1 = IDR   14,000</span></td>
		</tr>
			
	</table>
	</div>


	<table border="2px" width="100%" style="text-align: center;float: left;margin-top: 30px;">
		<tr>
			<td width="50%"><b>DETAIL OF CHANGES</b></td>
			<td width="30%"><b>UNIT RATE</b></td>
			<td width="20%"><b>AMOUNT</b></td>
		</tr>
		<?php
			$no = 0;
			$total_amount = 0;
			$query = $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '$data_inv[no_invoice]'")->result();
			foreach ($query as $key) {
				$no++;
				$amount= str_replace(".","",$key->amount);
				$total_amount +=  $amount;
		?>
		<tr>
			<td style="text-align: left;"><?php echo $key->deskripsi;?></td>
			<td style="text-align: center;"><?php echo str_replace(".","",$key->unit);?></td>
			<td style="text-align: right;">Rp. <?php echo $key->amount;?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="2"><b>TOTAL</b></td>
			<td><b>Rp. <?php echo number_format($total_amount);?></b></td>
		</tr>
	</table>


	<div style="float: left; width: 100%; margin-top: 50px;">
		<div class="col-md-6">
			<p style="text-align: center;">Surabaya, <?= TanggalIndo(date('Y-m-d')); ?></p>
			&nbsp;<br>
			&nbsp;<br>
			<p style="text-align: center;">Fitri Ayu Lestari</p>
		</div>
		<div class="col-md-6">
			<p style="text-align: left;">For Invooice Payment, Please Transfer to :<br>
				BNI a/n PT. HERSON PUTRA ATLANTIK<BR>
				Account No : 0316874619 (IDR)
			</p>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	window.print();
</script>
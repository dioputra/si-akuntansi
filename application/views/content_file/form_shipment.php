<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Shipment</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Master Shipment</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Jenis Shipmet (Pengiriman)</label>
                    <div class="col-sm-9">
                    <input type="text" id="jenis_shipment" value="<?php echo $jenis_shipment;?>" class="form-control">
                    <input type="hidden" id="id_shipment" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_akses()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/master_shipment');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_akses(){
              var jenis_shipment = $("#jenis_shipment").val();
              var id_shipment = $("#id_shipment").val();

              if (jenis_shipment == "") {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Silahkan lengkapi data agen.....!'
                        });
              }else{

                if (id_shipment == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_shipment');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "jenis_shipment" : jenis_shipment
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/master_shipment');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                    "url" : "<?php echo site_url('UserController/edit_shipment');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "jenis_shipment" : jenis_shipment,
                        "id_shipment" : id_shipment
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil diedit.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/master_shipment');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }
              }
          }

      </script>
<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Saldo Awal</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Saldo Awal</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                    <input type="text" id="nama" value="<?php echo $nama;?>" class="form-control">
                    <input type="hidden" id="id_bank" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">No. Rekening</label>
                    <div class="col-sm-9">
                    <input type="text" id="no_rekening" value="<?php echo $no_rekening;?>" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Periode Awal</label>
                    <div class="col-sm-9">
                    <input type="text" id="periode_awal" class="form-control" value="<?php echo $periode_awal;?>" placeholder="dd/mm/yyyy">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Periode Akhir</label>
                    <div class="col-sm-9">
                    <input type="text" id="periode_akhir" class="form-control" value="<?php echo $periode_akhir;?>" placeholder="dd/mm/yyyy">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Jumlah Saldo</label>
                    <div class="col-sm-9">
                    <input type="text" id="jumlah_saldo" class="form-control" value="<?php echo $jumlah_saldo;?>" placeholder="0">
                    </div>
                  </div>
                  
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_akses()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/data_bank');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_akses(){
              var nama_bank = $("#nama").val();
              var no_rekening = $("#no_rekening").val();
              var periode_awal = $("#periode_awal").val();
              var periode_akhir = $("#periode_akhir").val();
              var jumlah_saldo = $("#jumlah_saldo").val();
              var id_bank = $("#id_bank").val();

              if (nama_bank == "" || no_rekening == "" ||periode_awal == "" || periode_akhir == "" || jumlah_saldo == "") {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Silahkan lengkapi data bank.....!'
                        });
              }else{

                if (id_bank == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_bank');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama_bank" : nama_bank,
                        "no_rekening" : no_rekening,
                        "periode_awal" : periode_awal,
                        "periode_akhir" : periode_akhir,
                        "jumlah_saldo" : jumlah_saldo
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_bank');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                    "url" : "<?php echo site_url('UserController/edit_bank');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "nama_bank" : nama_bank,
                        "no_rekening" : no_rekening,
                        "periode_awal" : periode_awal,
                        "periode_akhir" : periode_akhir,
                        "jumlah_saldo" : jumlah_saldo,
                        "id_bank" : id_bank
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil diedit.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_bank');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }
              }
          }

      </script>
      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('jumlah_saldo');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>
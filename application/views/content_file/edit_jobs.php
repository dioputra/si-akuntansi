<!-- <?php error_reporting(0);?> -->
<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Jobs</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Master Jobs</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                    <form action="<?php echo site_url($action);?>" method="POST" >
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Job Number</label>
                    <div class="col-sm-5">
                        <input type="text" id="jobs_number" name="jobs_number" value="<?php echo $jobs_number;?>" class="form-control" readonly="readonly">
                        <input type="hidden" id="id_jobs" name="id_jobs" value="<?php echo $this->uri->segment(4);?>" class="form-control" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Tanggal</label>
                    <div class="col-sm-5">
                        <input type="text" id="tanggal" name="tanggal" value="<?php echo $tanggal;?>" class="form-control" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Shipment</label>
                    <div class="col-sm-9">
                      <select id="shipment" name="shipment" class="form-control single-select">
                          <?php
                            foreach ($shipment as $key) {
                          ?>
                              <option <?php if($v_shipment == $key->id_shipment) echo"selected";?> value="<?php echo $key->id_shipment;?>"><?php echo $key->jenis_shipment;?></option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Customer</label>
                    <div class="col-sm-9">
                      <select id="customer" name="customer" class="form-control single-select">
                          <?php
                            foreach ($customer as $key) {
                          ?>
                              <option <?php if($v_customer == $key->id_customer) echo"selected";?> value="<?php echo $key->id_customer;?>"><?php echo $key->nama;?></option>
                          <?php
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">No. BL</label>
                    <div class="col-sm-9">
                        <input type="text" id="no_bl" name="no_bl" value="<?php echo $no_bl;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Port Of load</label>
                    <div class="col-sm-9">
                        <input type="text" id="pol" name="pol" value="<?php echo $pol;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Vessel</label>
                    <div class="col-sm-9">
                        <input type="text" id="vessel" name="vessel" value="<?php echo $vessel;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Voyage</label>
                    <div class="col-sm-5">
                        <input type="text" id="voyage" name="voyage" value="<?php echo $voyage;?>" class="form-control" required="required">      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Destination</label>
                    <div class="col-sm-9">
                        <input type="text" id="destination" name="destination" value="<?php echo $destination;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Commodity</label>
                    <div class="col-sm-9">
                        <input type="text" id="commodity" name="commodity" value="<?php echo $commodity;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">CNTR Quantity</label>
                    <div class="col-sm-9">
                        <input type="text" id="cntr_quantity" name="cntr_quantity" value="<?php echo $cntr_quantity;?>" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Size</label>
                    <div class="col-sm-5">
                        <input type="text" id="size" name="size" value="<?php echo $size;?>" class="form-control" required="required">      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">ETD</label>
                    <div class="col-sm-5">
                        <input type="text" id="etd" name="etd" value="<?php echo $etd;?>" class="form-control" required="required">
                    </div>
                  </div>
                 <!--  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Date</label>
                    <div class="col-sm-5">
                        <input type="text" id="date" name="date" value="<?php echo $date;?>" class="form-control" required="required">
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Refund Shipper</label>
                    <div class="col-sm-5">
                        <input type="text" id="refund_shipper" name="refund_shipper" value="<?php echo $refund_shipper;?>" class="form-control" required="required">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Renmarsk</label>
                    <div class="col-sm-5">
                        <input type="text" id="renmarsk" name="renmarsk" value="<?php echo $renmarsk;?>" class="form-control" required="required">      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Freight expenses</label>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                           <table class="table table-striped" id="Tdeskripsi">
                              <thead>
                                <tr>
                                  <th scope="col">Deskripsi</th>
                                  <th scope="col">unit rate</th>
                                  <th scope="col">Debit</th>
                                  <th scope="col">Kredit</th>
                                  <th> Checklist</th>
                                  <th> Action</th>
                                </tr>
                              </thead>
                              <tbody id="loaddata">
                                
                              </tbody>
                              <tbody>
                                <tr>
                                    <td style="display: none;" width="40%"><input type="text" id="deskripsi" value="" name="deskripsi" class="form-control"></td>
                                    <td style="display: none;" width="20%"><input type="text" id="debit" value="0" name="debit" class="form-control"></td>
                                    <td style="display: none;" width="20%"><input type="text" id="kredit" value="0" name="kredit" class="form-control"></td>
                                    <td style="display: none;" width="10%"> 
                                     <input type="checkbox" id="myCheck" onclick="myFunction()">
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="4" width="90%"></td>
                                  <td><button type="button" class="btn btn-defult" onclick="adddes()">+</button></td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Operational</label>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                           <table class="table table-striped" id="Tdeskripsi">
                              <thead>
                                <tr>
                                  <th scope="col">Deskripsi</th>
                                  <th scope="col">Debit</th>
                                  <th scope="col">Kredit</th>
                                  <th> Checklist</th>
                                  <th> Action</th>
                                </tr>
                              </thead>
                              <tbody id="loaddataoper">
                                
                              </tbody>
                              <tbody>
                                <tr>
                                  <td style="display: none;" width="40%"><input type="text" id="desoper" value="" name="desoper" class="form-control"></td>
                                  <td style="display: none;" width="20%"><input type="text" id="debitoper" value="0" name="harga" class="form-control"></td>
                                  <td style="display: none;" width="20%"><input type="text" id="kreditoper" value="0" name="harga" class="form-control"></td>
                                  <td style="display: none;" width="10%"> </td>
                                </tr>
                                <tr>
                                  <td colspan="4" width="90%"></td>
                                  <td><button type="button" class="btn btn-defult" onclick="addoper()">+</button></td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Total</label>
                    <div class="col-sm-4">
                        <input type="text" id="total_debet" name="total"  placeholder="Rp.12.122xxx" value="<?php echo number_format($total_debet, 0,".",".");?>" class="form-control" readonly="">
                    </div>
                    <div class="col-sm-4">
                        <input type="text" id="total_kredit" name="total" value="<?php echo number_format($total_kredit, 0,".",".");?>" placeholder="Rp.12.122xxx" class="form-control" required="required" readonly="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Advance Received ON</label>
                    <div class="col-sm-5">
                        <input type="text" id="advance" name="advance" value="<?php echo $advance;?>" placeholder="Rp.12.122xxx" class="form-control" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Balance Due</label>
                    <div class="col-sm-5">
                        <input type="text" id="balance_due" name="balance" value="<?php echo number_format($balance, 0,".",".");?>" placeholder="Rp.12.122xxx" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Loss & Profit</label>
                    <div class="col-sm-5">
                        <input type="text" id="loos_profit" name="loss_profit" value="<?php echo number_format($loss_profit, 0,".",".") ;?>" placeholder="Rp.12.122xxx" class="form-control" required="required" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Note*</label>
                    <div class="col-sm-9">
                        <textarea id="date" name="note"  class="form-control" required="required"><?php echo $note;?></textarea>
                    </div>
                  </div>
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/master_jobs');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
                  </form>
              </div>
          </div>
        </div>
      </div><!--End Row-->
      <script src="<?php echo base_url(); ?>/assets/assets/js/jquery-3.2.1.min.js"></script>
      <script type="text/javascript">

          $(document).ready(function(){
              loaddata_all();
          });


          function loaddata_all(){
              var jobs = $("#jobs_number").val();

              var url_des = "<?php echo base_url(); ?>index.php/UserController/loaddatadesjob_edit/" ;
              $.ajax({
                 type: "POST",
                 url: url_des,
                 data: {jobs : jobs},
                 success: function(res_des)
                 {
                     $("#loaddata").html(res_des);
                 }
              });

              var url_op = "<?php echo base_url(); ?>index.php/UserController/loaddatadesjoboper_edit/" ;
              $.ajax({
                 type: "POST",
                 url: url_op,
                 data: {jobs : jobs},
                 success: function(res_oper)
                 {
                     $("#loaddataoper").html(res_oper);
                 }
              });
          }

          function adddes(){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/simpandesjob');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                      "no_jobs" : $("#jobs_number").val(),
                      "deskripsi" : $("#deskripsi").val(),
                      "debit" : $("#debit").val(),
                      "kredit" : $("#kredit").val()
                  },
                  success:function(data){
                    if (data.alert == "success") {
                        $("#deskripsi").val(data.deskripsi);
                        $("#debit").val("0");
                        $("#kredit").val("0");
                        $("#total_debet").val(data.total_debet);
                        $("#total_kredit").val(data.total_kredit);
                        $("#loos_profit").val(data.loss_profit);
                        loaddata_all();

                    }
                     
                  }
              });
          }

          function addoper(){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/simpan_joboper');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                      "no_jobs" : $("#jobs_number").val(),
                      "deskripsi" : $("#desoper").val(),
                      "debit" : $("#debitoper").val(),
                      "kredit" : $("#kreditoper").val()
                  },
                  success:function(data){
                    if (data.alert == "success") {
                        
                        $("#total_debet").val(data.total_debet);
                        $("#total_kredit").val(data.total_kredit);
                        $("#loos_profit").val(data.loss_profit);
                        $("#desoper").val(data.deskripsi);
                        $("#debitoper").val("0");
                        $("#kreditoper").val("0");
                        loaddata_all();

                    }
                     
                  }
              });
          }

          

          function balance(){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/balance');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                      "total_kredit" : $("#total_kredit").val(),
                      "advance" : $("#advance").val()
                  },
                  success:function(data){
                    if (data.alert == "success") {
                        $("#balance_due").val(data.balance);
                    }
                     
                  }
              });
          }

          function hapus(id_dea){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/hapus_des_jobs');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                    "id_des" : id_dea
                  },
                  success:function(hasil){
                      loaddata_all();
                  }
              });
          }

          
          function hapusoper(id){
              $.ajax({
                  "url" : "<?php echo site_url('UserController/hapus_des_oper');?>",
                  "type" : "POST",
                  "dataType" : "json",
                  "data" : {
                    "id_des" : id
                  },
                  success:function(hasil){
                      loaddata_all();
                     
                  }
              });
          }
      </script>
      <script type="text/javascript">
    
        var rupiah = document.getElementById('debit');
        rupiah.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah.value = formatRupiah(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>

      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('kredit');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>
       <script type="text/javascript">
    
        var rupiah3 = document.getElementById('harga');
        rupiah3.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah3.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah23(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>

      <script type="text/javascript">
    
        var rupiah3 = document.getElementById('harga');
        rupiah3.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah3.value = formatRupiah2(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah23(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>

      <script type="text/javascript">
    
        var kreditoperReg = document.getElementById('kreditoperReg');
        kreditoperReg.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          kreditoperReg.value = formatKreditoperReg(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatKreditoperReg(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>

      <script type="text/javascript">
    
        var debitoperReg = document.getElementById('debitoperReg');
        debitoperReg.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          debitoperReg.value = formatDebitoperReg(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatDebitoperReg(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
      </script>

      
<script type="text/javascript">


       function convertToRupiah(angka)
        {
          var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join('');
          for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
          return rupiah.split('',rupiah.length-1).reverse().join('');
        }


        function myFunction() {
          var checkBox = document.getElementById("myCheck");
          let renmarsk = $("#renmarsk").val().replace(/\./g , '');
          let debit = $("#debit").val().replace(/\./g , '');
          let hasil = debit*renmarsk;
          let sebelumnya = debit/renmarsk;

          if (checkBox.checked == true){
            $("#debit").val(convertToRupiah(hasil));
          } else {
             $("#debit").val(convertToRupiah(sebelumnya));
          }
        }

        function deskripdes_(id)
        {
              var url = "<?php echo base_url(); ?>index.php/UserController/deskripdes/"+id ;
            $.ajax({
               type: "POST",
               data: {deskripdes : $("#deskripdes"+id).val()},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }

        function debitdes_(id, datadebit)
        {   
            if(datadebit){
                var datades = datadebit;
            }else{
                var datades = $("#debitdes"+id).val();
            }

            var url = "<?php echo base_url(); ?>index.php/UserController/debitdes/"+id ;
            $.ajax({
               type: "POST",
               data: {debitdes : datades},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }

        function kreditdes_(id)
        {
              var url = "<?php echo base_url(); ?>index.php/UserController/kreditdes/"+id ;
            $.ajax({
               type: "POST",
               data: {kreditdes : $("#kreditdes"+id).val()},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }

        function desoperasional_(id)
        {
              var url = "<?php echo base_url(); ?>index.php/UserController/desoperasional/"+id ;
            $.ajax({
               type: "POST",
               data: {desoperasional : $("#desoperasional"+id).val()},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }

        function debitdesoper_(id, datadebit)
        {
            if(datadebit){
                var datades = datadebit;
            }else{
                var datades = $("#debitdesoper"+id).val();
            }

            var url = "<?php echo base_url(); ?>index.php/UserController/debitdesoper/"+id ;
            $.ajax({
               type: "POST",
               data: {debitdesoper : datades},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }

        function kreditdesoper_(id)
        {
              var url = "<?php echo base_url(); ?>index.php/UserController/kreditdesoper/"+id ;
            $.ajax({
               type: "POST",
               data: {kreditdesoper : $("#kreditdesoper"+id).val()},
               url: url,
               success: function(v)
               {
               }

            });
            return false;
        }
 

</script>
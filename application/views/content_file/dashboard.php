
      <div class="row mt-3">
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-info border-left-sm">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-info"><?php echo @$total_jobs;?></h4>
                <span>Job hari ini</span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-scooter">
                <i class="icon-briefcase text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-danger border-left-sm">
            <div class="card-body">
              <div class="media">
               <div class="media-body text-left">
                <h4 class="text-danger"><?php echo @$total_invoice;?></h4>
                <span>Invoice hari ini</span>
              </div>
               <div class="align-self-center w-circle-icon rounded-circle gradient-bloody">
                <i class="icon-book-open text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-success border-left-sm">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-success"><?php echo @$total_transaksi;?></h4>
                <span>Total Transaksi</span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-quepal">
                <i class="icon-basket-loaded text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card border-warning border-left-sm">
            <div class="card-body">
              <div class="media">
              <div class="media-body text-left">
                <h4 class="text-warning"><?php echo @$total_labarugi;?></h4>
                <span>Laba Rugi</span>
              </div>
              <div class="align-self-center w-circle-icon rounded-circle gradient-blooker">
                <i class="icon-graph text-white"></i></div>
            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
		  
		  
      

      <div class="row">
        <div class="col-12 col-lg-4 col-xl-4">
          <div class="card">
            <div class="card-header">
              Create Customer
            <div class="card-action">
             
             </div>
            </div>
            <div class="card-body">
              <center>
               <a href="<?php echo site_url('UserController/page_home/form_customer');?>"><img src="<?php echo base_url();?>assets/assets/images/customer.png" style="width: 100px;"></a>
               <p>Entry Customer</p>
              </center>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-4 col-xl-4">
            <div class="card">
              <div class="card-header">
                Create Job
              <div class="card-action">
               
               </div>
              </div>
              <div class="card-body">
                <center>
               <a href="<?php echo site_url('UserController/page_home/form_jobs');?>"><img src="<?php echo base_url();?>assets/assets/images/job.png" style="width: 100px;"></a>
               <p>Entry Jobs</p>
              </center>
              </div>
            </div>
        </div>
		<div class="col-12 col-lg-4 col-xl-4">
            <div class="card">
              <div class="card-header">
                Create Invoice
              <div class="card-action">
               
               </div>
              </div>
              <div class="card-body">
                <center>
               <a href="<?php echo site_url('UserController/page_home/form_invoice');?>"><img src="<?php echo base_url();?>assets/assets/images/invoice.png" style="width: 100px;"></a>
               <p>Entry Invoice</p>
              </center>
              </div>
            </div>
        </div>
      </div><!--End Row-->
	  <div class="row">
        <div class="col-lg-12">
          <div class="card">
		  <div class="card-header border-0">
                Transaksi Job
				<div class="card-action">
				 
                 </div>
                </div>
               <div class="table-responsive">
                 <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Shipment</th>
                        <th>Tgl</th>
                        <th>Job Number</th>
                        <th>Customer</th>
                        <th>No. BL</th>
                        <th>Port Of Load</th>
                        <th>CNTR Quantity</th>
                        <th>Total Debit</th>
                        <th>Total Kredit</th>
                        <th>Total Profit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($jobs as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo @$key->jenis_shipment;?></td>
                        <td><?php echo @$key->tanggal;?></td>
                        <td><?php echo @$key->no_job;?></td>
                        <td><?php echo @$key->nama;?></td>
                        <td><?php echo @$key->no_bl;?></td>
                        <td><?php echo @$key->p_o_l;?></td>
                        <td><?php echo @$key->cntr_quantity;?></td>
                        <td>
                          <?php
                            $total_debit = 0;
                            $total_kredit = 0;
                            $profit = 0;
                            $datadesjob = $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$key->no_job'")->result();
                            foreach ($datadesjob as $value) {
                              $total_debit += str_replace(".","",@$value->debit);
                              $total_kredit += str_replace(".","",@$value->kredit);
                            }
                            echo number_format($total_debit,0,".",".");
                            $profit = $total_debit - $total_kredit;
                          ?>
                        </td>
                        <td><?php echo number_format($total_kredit,0,".",".");?></td>
                        <td><?php echo number_format($profit,0,".",".");?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
               </div>
          </div>
        </div>
      </div><!--End Row-->
	  
       <!--End Dashboard Content-->
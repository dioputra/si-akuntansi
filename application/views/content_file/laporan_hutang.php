<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Laporan Hutang</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Hutang</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('export/laporanhutang/');?>" class="btn btn-outline-primary"><i class="fa fa-table mr-1"></i>Export Excel</a>
          </div>
        </div>

     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Hutang</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Nominal</th>
                        <th>Klien</th>
                        <th>Deskripsi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($hutang as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->tanggal_awal;?></td>
                        <td><?php echo $key->tgl_jatuh_tempo;?></td>
                        <td><?php echo "Rp ".number_format($key->nominal,2,',','.');?></td>
                        <td><?php echo $key->klien;?></td>
                        <td><?php echo $key->deskripsi;?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div>
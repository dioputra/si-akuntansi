<!DOCTYPE html>
<html>
<head>
	<title>Laporan Invoice 2</title>
	<style type="text/css">
		h3{
			margin-block-start: 0;
    		margin-block-end: 0;
			}

			.col-md-6{
				width: 50%;
				float: left;
			}
	</style>
</head>

<?php 
function TanggalIndo($date){
    $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);
 
    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;        
    return($result);
}
?>

<body style="font-family: segoe ui;font-size: 14px;">
	<center>
		<img src="<?php echo base_url();?>assets/assets/images/logo2.png" width="100%" height="150px">
		
		<br><br><h2>INVOICE</h2>
	</center>
	<table style="width: 100%;float: left;padding: 0px">
		<!-- <tr style="background: #ddd;height: 40px;">
			<td style="text-align: center;">SHIPPER :</td>
		</tr> -->
		<tr style="width: 49.5%; float: left; border: 2px solid #000;max-height: 110px;min-height: 110px;">
			<td style="height: 70px;"><h3><?php echo $data_inv['customer'];?></h3>
			<?php echo $data_inv['alamat'];?>
			</td>
		</tr>

		<tr style="width: 49.5%; float: left; border: 2px solid #000; border-left: none; max-height: 110px;min-height: 110px; background-color: #ffe7d3;">
			<td style="padding-top: 38px;padding-left: 30px;"><span style="font-size: 18px;"><strong>INVOICE &nbsp; </strong>  <strong> &nbsp; <?php echo $data_inv['no_invoice'];?></strong></span></td>
		</tr>

	</table>
	
	<div style="margin-top:30px;width: 99%;border: 2px solid #000;float: left;">

	<table width="50%" style="float: left;padding:0px 10px;">
		<tr>
			<td><strong>DATE</strong></td>
			<td><?php echo $data_inv['date'];?></td>
		</tr>
		<tr>
			<td><strong>QTY</strong></td>
			<td><?php echo $data_inv['qty'];?></td>
		</tr>
		<tr>
			<td><strong>MBL NO</strong></td>
			<td><?php echo $data_inv['mbl_no'];?></td>
		</tr>
		<tr>
			<td width="40%"><strong>VESSEL & VOY</strong></td>
			<td><?php echo $data_inv['vessel_voy'];?></td>
		</tr>
		<tr>
			<td><strong>JOB</strong></td>
			<td><?php echo $data_inv['no_jobs'];?></td>
		</tr>
		
		
		<tr>
			<td><strong>SIZE</strong></td>
			<td><?php echo $data_inv['size'];?></td>
		</tr>
	</table>

	<table width="50%">
			
			<tr>
				<td><strong>ATD</strong></td>
				<td><?php echo $data_inv['etd'];?></td>
			</tr>
			<tr>
				<td><strong>POL</strong></td>
				<td><?php echo $data_inv['p_o_l'];?></td>
			</tr>
			<tr>
				<td><strong>POD</strong></td>
				<td><?php echo $data_inv['pod'];?></td>
			</tr>
			<tr>
				<td><strong>COMMODITY</strong></td>
				<td><?php echo $data_inv['commodity'];?></td>
			</tr>
	</table>
	<table width="100%" style="border-top: 2px solid #000;">
			
		<tr>
			<td><strong>REMARKS</strong><span style="padding-left: 20px;">$1 = IDR   14,000</span></td>
		</tr>
			
	</table>

	</div>
	
	<table width="99%" style="text-align: center;float: left;margin-top: 30px;border: 2px solid #000;">
		<tr>
			<td width="50%" style="border-bottom: 2px solid #000;"><b>DETAIL OF CHANGES</b></td>
			<td width="30%" style="border-bottom: 2px solid #000;"><b>UNIT RATE</b></td>
			<td width="20%" style="border-bottom: 2px solid #000;border-left: 2px solid #000;"><b>AMOUNT (IDR)</b></td>
		</tr>
		<?php
			$no = 0;
			$total_amount = 0;
			$query = $this->db->query("SELECT * FROM tb_des_inv WHERE no_invo = '$data_inv[no_invoice]'")->result();
			foreach ($query as $key) {
				$no++;
				$amount= str_replace(".","",$key->amount);
				$total_amount +=  $amount;
		?>
		<tr>
			<td style="text-align: left;"><?php echo $key->deskripsi;?></td>
			<td style="text-align: center;"><?php echo str_replace(".","",$key->unit);?></td>
			<td style="text-align: right;border-left: 2px solid #000;">Rp. <?php echo $key->amount;?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="2" style="border-top: 2px solid #000;"><b>TOTAL</b></td>
			<td style="border-top: 2px solid #000;"><b>Rp. <?php echo number_format($total_amount);?></b></td>
		</tr>
	</table>
	

	<div style="float: left; width: 100%; margin-top: 50px;">
		<div class="col-md-6">
			<p style="text-align: center;">Surabaya, <?= TanggalIndo(date('Y-m-d')); ?></p>

			&nbsp;<br>
			&nbsp;<br>
			<p style="text-align: center;">Fitri Ayu Lestari</p>
		</div>
		<div class="col-md-6">
			<p style="text-align: left;">For Invooice Payment, Please Transfer to :<br>
				BNI a/n PT. HERSON PUTRA ATLANTIK<BR>
				Account No : 0316874619 (IDR)
			</p>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	window.print();
</script>

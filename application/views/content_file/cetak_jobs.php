<!DOCTYPE html>
<html>
<head>
	<title>Laporan Jobs</title>
</head>
<body style="font-family: segoe ui;font-size: 14px;">
	<center>
	<img src="<?php echo base_url();?>assets/assets/images/header.png" width="100%" height="150px">
	</center>
	<div style="width: 99.7%;float: left;text-align: center;border:2px solid #000;">
		<h3>TITLE INFORMATION</h3>
	</div>
	<table style="border: 2px solid #000;width: 50%;float: left;padding: 0px 10px;">
		<tr>
			<td>No. Jobs</td>
			<td>:</td>
			<td><?php echo $data_jobs['no_job'];?></td>
		</tr>
		<tr>
			<td>PORT OF LOAD</td>
			<td>:</td>
			<td><?php echo $data_jobs['p_o_l'];?></td>
		</tr>
		<tr>
			<td>No. BL</td>
			<td>:</td>
			<td><?php echo $data_jobs['no_bl'];?></td>
		</tr>
		<tr>
			<td>Vessel Name</td>
			<td>:</td>
			<td><?php echo $data_jobs['vessel'];?></td>
		</tr>
		<tr>
			<td>Destination</td>
			<td>:</td>
			<td><?php echo $data_jobs['destination'];?></td>
		</tr>
	</table>
	<table style="border: 2px solid #000;width: 50%;float: left;padding:0px 10px;">
		<tr>
			<td>Shipper</td>
			<td>:</td>
			<td><?php echo $data_jobs['nama'];?></td>
		</tr>
		<tr>
			<td>Commodity</td>
			<td>:</td>
			<td><?php echo $data_jobs['commodity'];?></td>
		</tr>
		<tr>
			<td>CNTR Quantity</td>
			<td>:</td>
			<td><?php echo $data_jobs['cntr_quantity'];?></td>
		</tr>
		<tr>
			<td>ETD</td>
			<td>:</td>
			<td><?php echo $data_jobs['etd'];?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td>:</td>
			<td><?php echo $data_jobs['date'];?></td>
		</tr>
	</table>
	<table border="2px" width="100%" style="text-align: center;">
		<tr>
			<td width="20%" rowspan="2">NO.</td>
			<td width="30%" rowspan="2">DESCRIPTION</td>
			<td width="50%" colspan="2">AMOUNT</td>
		</tr>
		<tr>
			<td width="25%">DEBIT</td>
			<td width="25%">KREDIT</td>
		</tr>
		<tr>
			<td><b>FREIGHT EXPENSES</b></td>
			<td colspan="4"></td>
		</tr>
		<?php
			$no = 0;
			$total_debit1 = 0;
			$total_kredit1 = 0;
			$query1 = $this->db->query("SELECT * FROM tb_des_job WHERE no_job = '$data_jobs[no_job]'")->result();
			foreach ($query1 as $key1) {
				$no++;
				$debit1= str_replace(".","",$key1->debit);
				$kredit1= str_replace(".","",$key1->kredit);
				$total_debit1 +=  $debit1;
				$total_kredit1 +=  $kredit1;
		?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $key1->deskripsi;?></td>
			<td style="text-align: right;">Rp. <?php echo $key1->debit;?></td>
			<td style="text-align: right;">Rp. <?php echo $key1->kredit;?></td>
		</tr>

		<?php } ?>


		<tr>
			<td><b>OPERATIONAL</b></td>
			<td colspan="4"></td>
		</tr>
		<?php
			$no = 0;
			$total_debit2 = 0;
			$total_kredit2 = 0;
			$query2 = $this->db->query("SELECT * FROM tb_job_opertaional WHERE no_job = '$data_jobs[no_job]'")->result();
			foreach ($query2 as $key2) {
				$no++;
				$debit2= str_replace(".","",$key2->debit);
				$kredit2= str_replace(".","",$key2->kredit);
				$total_debit2 +=  $debit2;
				$total_kredit2 +=  $kredit2;
		?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $key2->deskripsi;?></td>
			<td style="text-align: right;">Rp. <?php echo $key2->debit;?></td>
			<td style="text-align: right;">Rp. <?php echo $key2->kredit;?></td>
		</tr>

		<?php } ?>

		<tr>
			<td><b>OTHER EXPENSES</b></td>
			<td colspan="4"></td>
		</tr>
		<?php
			$no = 0;
			$total_debit3 = 0;
			$total_kredit3 = 0;
			$query3 = $this->db->query("SELECT * FROM tb_job_extra WHERE no_job = '$data_jobs[no_job]'")->result();
			foreach ($query3 as $key3) {
				$no++;
				$debit3= str_replace(".","",$key3->debit);
				$kredit3= str_replace(".","",$key3->kredit);
				$total_debit3 +=  $debit3;
				$total_kredit3 +=  $kredit3;
		?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $key3->deskripsi;?></td>
			<td style="text-align: right;">Rp. <?php echo $key3->debit;?></td>
			<td style="text-align: right;">Rp. <?php echo $key3->kredit;?></td>
		</tr>

		<?php } ?>

		<?php
			$total_kredit = $total_kredit1+$total_kredit2+$total_kredit3;
			$total_debit = $total_debit1+$total_debit2+$total_debit3;
		?>

		<tr>
			<td colspan="2"><b>TOTAL</b></td>
			<td><b>Rp. <?php echo number_format($total_debit1+$total_debit2+$total_debit3);?></b></td>
			<td><b>Rp. <?php echo number_format($total_kredit1+$total_kredit2+$total_kredit3);?></b></td>
		</tr>
		<tr>
			<td colspan="2"><b>ADVANCE RECEIVED ON <?php echo $data_jobs['etd'];?></b></td>
			<td colspan="2" style="text-align: right;"><b>Rp. <?php echo $data_jobs['advance_received_on'];?></b></td>
		</tr>
		<tr>
			<td colspan="2"><b>BALANCE DUE</b></td>
			<td colspan="2" style="text-align: right;">
				<b>
					<?php
						$advance = str_replace(".","",$data_jobs['advance_received_on']);
						$balance_due = $total_kredit - $advance;
						echo "Rp. ".number_format($balance_due);
					?>
				</b>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>LOSS & PROFIT</b></td>
			<td colspan="2" style="text-align: right;">
				<b>
					<?php
						$loss_profit = $total_debit - $total_kredit;
						echo "Rp. ".number_format($loss_profit);
					?>
				</b>
			</td>
		</tr>
	</table>
	<div style="width: 95%;float: left;border:2px solid #808080;margin-top: 20px;padding: 10px 2.4%">
		<h3>Note :</h3>
		<p><?php echo $data_jobs['note'];?></p>
	</div>
	<table width="80%" border="1px" style="text-align: center;margin-top: 30px;float: left;">
		<tr>
			<td>Issued by,</td>
			<td>Checked by,</td>
			<td>Verified by,</td>
			<td>Acknowledge by,</td>
		</tr>
		<tr>
			<td height="70px"></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Finance</td>
			<td>Director</td>
			<td>Finance Head</td>
			<td>President Director</td>
		</tr>
		<tr>
			<td>Fitria Ayu</td>
			<td>Nurul Fadilah</td>
			<td>Hanung</td>
			<td>Yudi Sansono</td>
		</tr>
	</table>
</body>
</html>
<!-- <script type="text/javascript">
	window.print();
</script> -->
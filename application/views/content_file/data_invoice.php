<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Invoice</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Jobs</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invoice</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Invoice</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No. Invoice</th>
                        <th>Tgl</th>
                        <th>Job Number</th>
                        <th>Customer</th>
                        <th>No. MBL</th>
                        <th>VESSEL & VOY</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($invoice as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->no_invoice;?></td>
                        <td><?php echo $key->date;?></td>
                        <td><?php echo $key->no_job;?></td>
                        <td><?php echo $key->customer;?></td>
                        <td><?php echo $key->mbl_no;?></td>
                        <td><?php echo $key->vessel_voy;?></td>
                        <td>
                            <a href="<?php echo site_url('UserController/cetak_invoice/');?><?php echo $key->id_invoice;?>" class="btn btn-outline-success" title="Edit" target="_blank"><i class="fa fa-print"></i> Invoice 1</a>
                             <a href="<?php echo site_url('UserController/cetak_invoice_2/');?><?php echo $key->id_invoice;?>" class="btn btn-outline-success" title="Edit" target="_blank"><i class="fa fa-print"></i> Invoice 2</a>
                           <!-- <a href="<?php echo site_url('UserController/page_home/edit_invoice/');?><?php echo $key->id_invoice;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a> -->
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_invoice;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_invoice){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_invoice');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_invoice" : id_invoice
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/data_invoice');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


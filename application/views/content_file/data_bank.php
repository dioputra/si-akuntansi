<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Saldo Awal</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Saldo Awal</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_bank');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Saldo Awal</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Bank</th>
                        <th>No. Rekening</th>
                        <th>Periode Awal</th>
                        <th>Periode Akhir</th>
                        <th>Jumlah Saldo</th>
                        <th>Sisa Saldo</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($bank as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->nama_bank;?></td>
                        <td><?php echo $key->no_rekening;?></td>
                        <td><?php echo $key->periode_awal;?></td>
                        <td><?php echo $key->periode_akhir;?></td>
                        <td><?php echo $key->jumlah_saldo;?></td>
                        <td><?php echo $key->sisa_saldo;?></td>
                        <td>
                            <a href="<?php echo site_url('UserController/page_home/form_bank/');?><?php echo $key->id_bank;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_bank;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_bank){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_bank');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_bank" : id_bank
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/data_bank');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


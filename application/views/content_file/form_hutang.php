<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input Hutang</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Hutang</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Tanggal Awal</label>
                    <div class="col-sm-9">
                    <input type="text" id="tgl_awal" value="<?php echo $tgl_awal;?>" class="form-control">
                    <input type="hidden" id="id_hutang" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Tanggal Jatuh Tempo</label>
                    <div class="col-sm-9">
                    <input type="text" id="tgl_jatuh_tempo" value="<?php echo $tgl_jatuh_tempo;?>" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Nominal</label>
                    <div class="col-sm-9">
                    <input type="text" id="nominal" class="form-control" value="<?php echo $nominal;?>" >
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                    <input type="text" id="deskripsi" class="form-control" value="<?php echo $deskripsi;?>" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Klien</label>
                    <div class="col-sm-9">
                    <input type="text" id="klien" class="form-control" value="<?php echo $klien;?>" >
                    </div>
                  </div>
                  
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_hutang()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/data_hutang');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_hutang(){
              var tgl_awal = $("#tgl_awal").val();
              var tgl_jatuh_tempo = $("#tgl_jatuh_tempo").val();
              var nominal = $("#nominal").val();
              var deskripsi = $("#deskripsi").val();
              var klien = $("#klien").val();
              var id_hutang = $("#id_hutang").val();

              if (tgl_awal == "" || tgl_jatuh_tempo == "" ||nominal == "" || deskripsi == "" || klien == "") {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Silahkan lengkapi data hutang.....!'
                        });
              }else{

                if (id_hutang == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_hutang');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "tgl_awal" : tgl_awal,
                        "tgl_jatuh_tempo" : tgl_jatuh_tempo,
                        "nominal" : nominal,
                        "deskripsi" : deskripsi,
                        "klien" : klien
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/data_hutang');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                      "url" : "<?php echo site_url('UserController/edit_hutang');?>",
                      "type" : "POST",
                      "dataType" : "json",
                      "data" : {
                          "tgl_awal" : tgl_awal,
                          "tgl_jatuh_tempo" : tgl_jatuh_tempo,
                          "nominal" : nominal,
                          "deskripsi" : deskripsi,
                          "klien" : klien,
                          "id_hutang" : id_hutang
                      },
                      success:function(data){
                          if (data.alert == "success") {
                              swal.fire({
                                  type: 'success',
                                  title: 'Success',
                                  text: 'Data berhasil diedit.....!'
                              })
                              .then((value) => {
                                  document.location = "<?php echo site_url('UserController/page_home/data_hutang');?>";
                              });
                          }else{
                               swal.fire({
                                    type: 'error',
                                    title: 'Failed',
                                    text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                                });
                          }
                      }

                  });
                }
              }
          }

      </script>
      <script type="text/javascript">
    
        var rupiah2 = document.getElementById('nominal');
        rupiah2.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah2.value = formatRupiah2(this.value, ' ');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah2(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? ' ' + rupiah : '');
        }
      </script>
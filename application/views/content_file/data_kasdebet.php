<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Kas Debet</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kas Debet</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_kas_debet');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Kas Debet</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tgl</th>
                        <th>Deskripsi</th>
                        <th>Nama Bank</th>
                        <th>No. Rekening</th>
                        <th>Nominal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($kdebet as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->tgl_transaksi;?></td>
                        <td><?php echo $key->deskripsi;?></td>
                        <td><?php echo $key->nama_bank;?></td>
                        <td><?php echo $key->no_rekening;?></td>
                        <td><?php echo $key->nominal;?></td>
                        <td>
                            <a href="<?php echo site_url('UserController/page_home/form_kas_debet/');?><?php echo $key->id_kas_debet;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_kas_debet;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_kas){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_kas_debet');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_kas" : id_kas
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/data_kasdebet');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


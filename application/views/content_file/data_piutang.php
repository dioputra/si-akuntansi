<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Data Piutang</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Piutang</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_piutang');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Piutang</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Nominal</th>
                        <th>Klien</th>
                        <th>Deskripsi</th>
                        <th>Status</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;  $nominal = [];
                        foreach ($piutang as $key) {
                            $no++;  $nominal[] = $key->nominal;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->tgl_awal;?></td>
                        <td><?php echo $key->tgl_jatuh_tempo;?></td>
                        <td><?php echo $key->nominal;?></td>
                        <td><?php echo $key->klien;?></td>
                        <td><?php echo $key->deskripsi;?></td>
                        <td><?php echo $key->status;?></td>
                        <td>
                            <button type="button" class="btn btn-outline-success" onclick="bayar(<?php echo $key->id_piutang;?>)" title="Bayar"><i class="fa fa-money"></i> Bayar</button>
                            <a href="<?php echo site_url('UserController/page_home/form_piutang/');?><?php echo $key->id_piutang;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_piutang;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } $sum_nominal = array_sum($nominal); ?>
                    <tr>
                      <td colspan="2" style="text-align: center;">TOTAL</td>
                      <td colspan="2" style="text-align: center;"><?= $sum_nominal ?></td>
                      <td colspan="4"></td>
                    </tr>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

<script type="text/javascript">
    function bayar(id_piutang){
        Swal.fire({
          title: 'Apakah anda yakin ?',
          text: "",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then((result) => {
          if (result.value) {
            $.ajax({
                "url" : "<?php echo site_url('UserController/bayar_piutang');?>",
                "type" : "POST",
                "dataType" : "json",
                "data" : {
                    "id_piutang" : id_piutang
                },
                success:function(data){
                    if (data.alert == "success") {
                        swal.fire({
                            type: 'success',
                            title: 'Success',
                            text: 'Data berhasil dibayar.....!'
                        })
                        .then((value) => {
                            document.location = "<?php echo site_url('UserController/page_home/data_piutang');?>";
                        });
                    }else{
                         swal.fire({
                              type: 'error',
                              title: 'Failed',
                              text: 'Data gagal dibayar.....!'
                          });
                    }
                }

            });
          }
        })
    }

    function hapus_akses(id_piutang){
        Swal.fire({
          title: 'Apakah anda yakin?',
          text: "Kamu ingin menghapus data ini!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'yakin!'
        }).then((result) => {
          if (result.value) {
            $.ajax({
                "url" : "<?php echo site_url('UserController/hapus_piutang');?>",
                "type" : "POST",
                "dataType" : "json",
                "data" : {
                    "id_piutang" : id_piutang
                },
                success:function(data){
                    if (data.alert == "success") {
                        swal.fire({
                            type: 'success',
                            title: 'Success',
                            text: 'Data berhasil di hapus.....!'
                        })
                        .then((value) => {
                            document.location = "<?php echo site_url('UserController/page_home/data_piutang');?>";
                        });
                    }else{
                         swal.fire({
                              type: 'error',
                              title: 'Failed',
                              text: 'Data gagal dihapus.....!'
                          });
                    }
                }

            });
          }
        })
    }
</script>


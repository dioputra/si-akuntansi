<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Master Jobs</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Jobs</a></li>
            <li class="breadcrumb-item active" aria-current="page">Master Jobs</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_jobs');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Jobs</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Shipment</th>
                        <th>Tgl</th>
                        <th>Job Number</th>
                        <th>Customer</th>
                        <th>No. BL</th>
                        <th>Port Of Load</th>
                        <th>CNTR Quantity</th>
                        <th>refund shipper</th>
                        <th>voyage</th>
                        <th>renmarsk</th>
                        <th>action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($jobs as $key) {
                            $no++;

                          $datashipement = $this->db->query("SELECT * FROM tb_shipment WHERE id_shipment = '$key->id_shipment'")->row();
                          $totalshipment = $this->db->query("SELECT * FROM tb_shipment WHERE id_shipment = '$key->id_shipment'")->num_rows();
                          if ($totalshipment > 0) {
                                $Shipment = $datashipement->jenis_shipment;
                          }else{
                                $Shipment = "";
                          }
                          $datacustomer = $this->db->query("SELECT * FROM tb_customer WHERE id_customer = '$key->id_customer'")->row();
                          $totalcustomer = $this->db->query("SELECT * FROM tb_customer WHERE id_customer = '$key->id_customer'")->num_rows();
                          if ($totalcustomer > 0) {
                                $Customer = $datacustomer->nama;
                          }else{
                                $Customer = "";
                          }
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $Shipment;?></td>
                        <td><?php echo $key->tanggal;?></td>
                        <td><?php echo $key->no_job;?></td>
                        <td><?php echo $Customer;?></td>
                        <td><?php echo $key->no_bl;?></td>
                        <td><?php echo $key->p_o_l;?></td>
                        <td><?php echo $key->cntr_quantity;?></td>
                        <td><?= $key->refund_shipper?></td>
                        <td><?= $key->voyage?></td>
                        <td><?= $key->renmarsk?></td>
                        <td>
                            <a href="<?php echo site_url('UserController/page_home/form_invoice/');?><?php echo $key->id_jobs;?>" class="btn btn-outline-warning" title="Invoice"><i class="fa fa-file"></i> Invoice</a>
                            <a href="<?php echo site_url('UserController/cetak_jobs/');?><?php echo $key->id_jobs;?>" class="btn btn-outline-success" title="Edit" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                            <a href="<?php echo site_url('UserController/page_home/edit_jobs/');?><?php echo $key->id_jobs;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_jobs;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Shipment</th>
                        <th>Tgl</th>
                        <th>Job Number</th>
                        <th>Customer</th>
                        <th>No. BL</th>
                        <th>Port Of Load</th>
                        <th>CNTR Quantity</th>
                        <th>refund shipper</th>
                        <th>action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_jobs){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_jobs');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_jobs" : id_jobs
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/master_jobs');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Data Hutang</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Hutang</li>
         </ol>
       </div>

       <div class="col-sm-3">
           <div class="btn-group float-sm-right">
                <a href="<?php echo site_url('UserController/page_home/form_hutang');?>" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Tambah Data</a>
                
          </div>
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Hutang</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Nominal</th>
                        <th>Klien</th>
                        <th>Deskripsi</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0; $nominal = [];
                        foreach ($hutang as $key) {
                            $no++; $nominal[] = $key->nominal;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->tanggal_awal;?></td>
                        <td><?php echo $key->tgl_jatuh_tempo;?></td>
                        <td><?php echo $key->nominal;?></td>
                        <td><?php echo $key->klien;?></td>
                        <td><?php echo $key->deskripsi;?></td>
                        <td>
                            <button type="button" class="btn btn-outline-success"  title="Hapus"><i class="fa fa-money"></i> Bayar</button>
                            <a href="<?php echo site_url('UserController/page_home/form_hutang/');?><?php echo $key->id_hutang;?>" class="btn btn-outline-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                            <button type="button" onclick="hapus_akses(<?php echo $key->id_hutang;?>)" class="btn btn-outline-danger"  title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                <?php } $sum_nominal = array_sum($nominal); ?>
                    <tr>
                      <td colspan="2" style="text-align: center;">TOTAL</td>
                      <td colspan="2" style="text-align: center;"><?= $sum_nominal ?></td>
                      <td colspan="3"></td>
                    </tr>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <script type="text/javascript">
          function hapus_akses(id_hutang){
                Swal.fire({
                  title: 'Apakah anda yakin?',
                  text: "Kamu ingin menghapus data ini!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'yakin!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax({
                        "url" : "<?php echo site_url('UserController/hapus_hutang');?>",
                        "type" : "POST",
                        "dataType" : "json",
                        "data" : {
                            "id_hutang" : id_hutang
                        },
                        success:function(data){
                            if (data.alert == "success") {
                                swal.fire({
                                    type: 'success',
                                    title: 'Success',
                                    text: 'Data berhasil di hapus.....!'
                                })
                                .then((value) => {
                                    document.location = "<?php echo site_url('UserController/page_home/data_hutang');?>";
                                });
                            }else{
                                 swal.fire({
                                      type: 'error',
                                      title: 'Failed',
                                      text: 'Data gagal dihapus.....!'
                                  });
                            }
                        }

                    });
                  }
                })
                
          }
      </script>


<!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Kas Debet</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kas Debet</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> List Kas Debet</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tgl</th>
                        <th>Deskripsi</th>
                        <th>Nama Bank</th>
                        <th>No. Rekening</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        foreach ($kdebet as $key) {
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?>.</td>
                        <td><?php echo $key->tgl_transaksi;?></td>
                        <td><?php echo $key->deskripsi;?></td>
                        <td><?php echo $key->nama_bank;?></td>
                        <td><?php echo $key->no_rekening;?></td>
                        <td><?php echo $key->nominal;?></td>
                        
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->



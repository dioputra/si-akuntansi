<div class="row pt-2 pb-2">
    <div class="col-sm-9">
		    <h4 class="page-title">Form Input User</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Home</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">User</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
         </ol>
	   </div>
</div>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">Form Input</div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Username</label>
                    <div class="col-sm-9">
                    <input type="text" id="username" value="<?php echo $username;?>" class="form-control">
                    <input type="hidden" id="id_akses" value="<?php echo $this->uri->segment(4);?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="basic-input" class="col-sm-3 col-form-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                    <input type="text" id="nama" value="<?php echo $nama;?>" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="placeholder-input" class="col-sm-3 col-form-label">Password</label>
                    <div class="col-sm-9">
                    <input type="password" id="password" class="form-control" value="<?php echo $password;?>" placeholder="*********">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="disabled-input" class="col-sm-3 col-form-label">Confirmasi Password</label>
                    <div class="col-sm-9">
                    <input type="password" id="conpass" class="form-control" value="<?php echo $password;?>" placeholder="**********">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="readonly-input" class="col-sm-3 col-form-label">Level</label>
                    <div class="col-sm-9">
                      <select id="level" class="form-control">
                          <option <?php if($level == "Admin") echo "selected"; ?>>Admin</option>
                          <option <?php if($level == "Staff") echo "selected"; ?>>Staff</option>
                          <option <?php if($level == "Manager") echo "selected"; ?>>Manager</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mt-4">
                    <label for="staticEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button type="button" onclick="simpan_akses()" class="btn btn-primary shadow-primary px-5"><i class="fa fa-save"></i> <?php echo $btn;?></button>
                        <a href="<?php echo site_url('UserController/page_home/hak_akses');?>" class="btn btn-danger shadow-primary px-5"><i class="fa fa-reply"></i> Kembali</a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div><!--End Row-->

      <script type="text/javascript">
          
          function simpan_akses(){
              var username = $("#username").val();
              var nama = $("#nama").val();
              var password = $("#password").val();
              var conpass = $("#conpass").val();
              var level = $("#level").val();
              var id_akses = $("#id_akses").val();

              if (password != conpass) {
                  swal.fire({
                            type: 'warning',
                            title: 'Failed',
                            text: 'Password & confirmasi password tidak sama.....!'
                        });
              }else{

                if (id_akses == "") {
                  $.ajax({
                    "url" : "<?php echo site_url('UserController/simpan_akses');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "username" : username,
                        "password" : password,
                        "nama" : nama,
                        "level" : level
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil tersimpan.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/hak_akses');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }else{
                    $.ajax({
                    "url" : "<?php echo site_url('UserController/edit_akses');?>",
                    "type" : "POST",
                    "dataType" : "json",
                    "data" : {
                        "username" : username,
                        "password" : password,
                        "nama" : nama,
                        "level" : level,
                        "id_akses" : id_akses
                    },
                    success:function(data){
                        if (data.alert == "success") {
                            swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data berhasil diedit.....!'
                            })
                            .then((value) => {
                                document.location = "<?php echo site_url('UserController/page_home/hak_akses');?>";
                            });
                        }else{
                             swal.fire({
                                  type: 'error',
                                  title: 'Failed',
                                  text: 'Data yang anda masukkan salah, silahkan cek kembali.....!'
                              });
                        }
                    }

                  });
                }
              }
          }

      </script>
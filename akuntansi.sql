/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : akuntansi

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-01 09:23:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_agen
-- ----------------------------
DROP TABLE IF EXISTS `tb_agen`;
CREATE TABLE `tb_agen` (
  `id_agen` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date_create` date NOT NULL,
  PRIMARY KEY (`id_agen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_agen
-- ----------------------------

-- ----------------------------
-- Table structure for tb_akses
-- ----------------------------
DROP TABLE IF EXISTS `tb_akses`;
CREATE TABLE `tb_akses` (
  `id_akses` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  PRIMARY KEY (`id_akses`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_akses
-- ----------------------------
INSERT INTO `tb_akses` VALUES ('1', 'admin', 'Administrator', 'admin', 'Admin');

-- ----------------------------
-- Table structure for tb_bank
-- ----------------------------
DROP TABLE IF EXISTS `tb_bank`;
CREATE TABLE `tb_bank` (
  `id_bank` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(100) NOT NULL,
  `no_rekening` varchar(100) NOT NULL,
  `periode_awal` varchar(100) NOT NULL,
  `periode_akhir` varchar(100) NOT NULL,
  `jumlah_saldo` varchar(100) NOT NULL,
  `sisa_saldo` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_bank
-- ----------------------------
INSERT INTO `tb_bank` VALUES ('3', 'KAS BANK', '717171', '01/10/2019', '31/10/2019', '50.000.009', '36.040.009', '2019-10-19');

-- ----------------------------
-- Table structure for tb_customer
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer`;
CREATE TABLE `tb_customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date_create` date NOT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_customer
-- ----------------------------
INSERT INTO `tb_customer` VALUES ('2', 'PT. TRIMEGA ', 'SURABAYA', ' 018181818', 'admin@trimega.com', '2019-10-10');
INSERT INTO `tb_customer` VALUES ('3', 'CV. RIZKY JAYA INDOSOFT', 'gresik', ' 818318', 'admin@rizkyjaya.com', '2019-10-10');

-- ----------------------------
-- Table structure for tb_des_inv
-- ----------------------------
DROP TABLE IF EXISTS `tb_des_inv`;
CREATE TABLE `tb_des_inv` (
  `id_des_inv` int(11) NOT NULL AUTO_INCREMENT,
  `no_job` varchar(100) NOT NULL,
  `no_invo` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_des_inv`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_des_inv
-- ----------------------------
INSERT INTO `tb_des_inv` VALUES ('3', '0', 'AJI/INV/1/19/09/2019', 'sdasdas', '12', '10000', null);
INSERT INTO `tb_des_inv` VALUES ('4', '0', 'AJI/INV/1/19/09/2019', '2323', '1', '10000', null);
INSERT INTO `tb_des_inv` VALUES ('5', '0', 'AJI/INV/1/02/10/2019', '1asdasddasd', '1', '200000', null);
INSERT INTO `tb_des_inv` VALUES ('6', '0', 'AJI/INV/2/02/10/2019', 'adsfasdf121', '11', '12000000', null);
INSERT INTO `tb_des_inv` VALUES ('8', '0', 'AJI/INV/4/19/10/2019', 'Dio Charge', '4', '4500000', null);
INSERT INTO `tb_des_inv` VALUES ('9', '0', 'AJI/INV/6/28/10/2019', 'afafafa', '1', '10000', null);
INSERT INTO `tb_des_inv` VALUES ('10', '0', 'AJI/INV/6/28/10/2019', 'qrqrqrqrq', '2', '200000', null);
INSERT INTO `tb_des_inv` VALUES ('11', '0', 'AJI/INV/9/29/10/2019', 'afafafa', '4', '800000', null);
INSERT INTO `tb_des_inv` VALUES ('23', '0', 'AJI/INV/1/25/01/2020', 'Hahahahaa', '0', '360', null);
INSERT INTO `tb_des_inv` VALUES ('27', '0', 'AJI/INV/2/25/01/2020', '', '0', '1200', null);
INSERT INTO `tb_des_inv` VALUES ('29', '0', 'AJI/INV/3/27/01/2020', '', '0', '1720661', null);
INSERT INTO `tb_des_inv` VALUES ('32', 'JOBS/1/19/10/2019', 'AJI/INV/4/30/01/2020', '', '', '20461', '2020-01-30 00:00:00');

-- ----------------------------
-- Table structure for tb_des_job
-- ----------------------------
DROP TABLE IF EXISTS `tb_des_job`;
CREATE TABLE `tb_des_job` (
  `id_des_job` int(11) NOT NULL AUTO_INCREMENT,
  `no_job` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `debit` varchar(100) NOT NULL,
  `kredit` varchar(100) NOT NULL,
  `unitrate` varchar(100) NOT NULL,
  PRIMARY KEY (`id_des_job`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_des_job
-- ----------------------------
INSERT INTO `tb_des_job` VALUES ('86', 'JOBS/1/07/10/2019', 'PEMBAYARAN SISA PROGRAM AKUTANSI', '100.000', '30.000', '');
INSERT INTO `tb_des_job` VALUES ('97', 'JOBS/1/10/10/2019', 'PEMBAYARAN SISA PROGRAM AKUTANSI', '10.000.000', '100.000.000', '');
INSERT INTO `tb_des_job` VALUES ('98', 'JOBS/1/10/10/2019', 'TITIPAN SHIPPING $50/CNTR', '10.000.000.000', '1.000.000.000', '');
INSERT INTO `tb_des_job` VALUES ('99', 'JOBS/1/12/10/2019', 'TESTING', '1.000.000', '500.000', '');
INSERT INTO `tb_des_job` VALUES ('101', 'JOBS/1/12/10/2019', 'bghg', '1.000.000', '500.000', '');
INSERT INTO `tb_des_job` VALUES ('104', 'JOBS/1/19/10/2019', 'aaaaa', '30.000', '4.000.000', '');
INSERT INTO `tb_des_job` VALUES ('105', 'JOBS/1/19/10/2019', 'aaaa', '30.000', '200.000', '');
INSERT INTO `tb_des_job` VALUES ('141', 'JOBS/1/03/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('142', 'JOBS/1/03/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('143', 'JOBS/2/07/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('144', 'JOBS/2/07/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('149', 'JOBS/1/19/10/2019', '', '200.000', '100000', '');
INSERT INTO `tb_des_job` VALUES ('150', 'JOBS/1/19/10/2019', '', '200.000', '20000', '');
INSERT INTO `tb_des_job` VALUES ('151', 'JOBS/1/19/10/2019', '', '1.000.000', '0', '');
INSERT INTO `tb_des_job` VALUES ('152', 'JOBS/1/19/10/2019', '', '20000', '0', '');
INSERT INTO `tb_des_job` VALUES ('155', 'JOBS/3/20/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('157', 'JOBS/3/25/01/2020', 'asdasd', '1000', '1', '');
INSERT INTO `tb_des_job` VALUES ('158', 'JOBS/4/25/01/2020', 'dio Putra', '2000', '2', '');
INSERT INTO `tb_des_job` VALUES ('160', 'JOBS/5/25/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('161', 'JOBS/6/25/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('162', 'JOBS/7/25/01/2020', '', '0', '0', '');
INSERT INTO `tb_des_job` VALUES ('163', 'JOBS/8/25/01/2020', 'dio Putra', '10.000', '10', '');
INSERT INTO `tb_des_job` VALUES ('164', 'JOBS/9/25/01/2020', 'diohaw', '100', '1', '');
INSERT INTO `tb_des_job` VALUES ('167', 'JOBS/10/25/01/2020', 'Monika', '120', '20', '');
INSERT INTO `tb_des_job` VALUES ('169', 'JOBS/10/25/01/2020', 'Abc', '20', '10', '');
INSERT INTO `tb_des_job` VALUES ('182', 'JOBS/11/27/01/2020', 'ssdfsdf', '2', '0', '');
INSERT INTO `tb_des_job` VALUES ('195', 'JOBS/12/27/01/2020', '', '576', '0', '2');
INSERT INTO `tb_des_job` VALUES ('227', 'JOBS/12/28/01/2020', 'abc', '288', '1.000', '2');
INSERT INTO `tb_des_job` VALUES ('228', 'JOBS/12/28/01/2020', 'def', '2.4', '100', '2');
INSERT INTO `tb_des_job` VALUES ('229', 'JOBS/13/28/01/2020', '', '0', '0', '');

-- ----------------------------
-- Table structure for tb_hutang
-- ----------------------------
DROP TABLE IF EXISTS `tb_hutang`;
CREATE TABLE `tb_hutang` (
  `id_hutang` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_awal` varchar(100) NOT NULL,
  `tgl_jatuh_tempo` varchar(100) NOT NULL,
  `nominal` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `klien` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_hutang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_hutang
-- ----------------------------
INSERT INTO `tb_hutang` VALUES ('1', '10-10-2020', '10-10-2021', '10000000000', 'Lorem Ipsum Dio', 'Sunarto', 'Jomblo', '0000-00-00');
INSERT INTO `tb_hutang` VALUES ('2', '10-10-2020', '10-10-2021', '20000000000', 'sddasdasd', 'Sunartosdoisdoisa', 'Jomblo', '0000-00-00');

-- ----------------------------
-- Table structure for tb_invoice
-- ----------------------------
DROP TABLE IF EXISTS `tb_invoice`;
CREATE TABLE `tb_invoice` (
  `id_invoice` int(11) NOT NULL AUTO_INCREMENT,
  `no_list` int(11) NOT NULL,
  `no_invoice` varchar(100) NOT NULL,
  `no_jobs` varchar(100) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `mbl_no` varchar(100) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `size` varchar(50) NOT NULL,
  `vessel_voy` varchar(250) NOT NULL,
  `etd` varchar(250) NOT NULL,
  `pod` varchar(250) NOT NULL,
  `commodity` varchar(250) NOT NULL,
  `create_date` date NOT NULL,
  `type_pembayaran` varchar(100) NOT NULL,
  `remarks` varchar(20) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_invoice
-- ----------------------------
INSERT INTO `tb_invoice` VALUES ('6', '1', 'AJI/INV/1/19/09/2019', 'JOBS/1/18/09/2019', 'Brohid Office', '05/08/2019', '1111', '234', '2', 'asdf', '09/25/2019', 'asdf', 'asdf', '2019-09-19', 'Cash', '0', null);
INSERT INTO `tb_invoice` VALUES ('8', '2', 'AJI/INV/2/02/10/2019', 'JOBS/1/18/09/2019', 'Brohid Office', '09/26/2019', 'asd', '12', '100', 'asdf', '09/25/2019', 'asdf', 'asdf', '2019-10-05', 'Kredit', '0', null);
INSERT INTO `tb_invoice` VALUES ('9', '3', 'AJI/INV/3/12/10/2019', 'JOBS/1/12/10/2019', 'PT. TRIMEGA ', '12/10/2019', '999999', '2', '2', 'KAPAL API', '11/10/2019', 'JAKARTA', 'BATERAY', '2019-10-12', 'Kredit', '0', '2');
INSERT INTO `tb_invoice` VALUES ('10', '4', 'AJI/INV/4/19/10/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '17/10/2019', '7777', '2', '1', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-12-12', 'Kredit', '0', '2');
INSERT INTO `tb_invoice` VALUES ('11', '5', 'AJI/INV/5/28/10/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '30/11/2019', '7777', '2', '1', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-10-28', 'Kredit', '0', '2');
INSERT INTO `tb_invoice` VALUES ('12', '6', 'AJI/INV/6/28/10/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '01/11/2019', '0808', '100', '2', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-10-28', 'Cash', '0', '2');
INSERT INTO `tb_invoice` VALUES ('13', '7', 'AJI/INV/7/28/10/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '23/10/2019', '8787', '100', '100', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-10-28', 'Kredit', '0', '2');
INSERT INTO `tb_invoice` VALUES ('14', '8', 'AJI/INV/8/28/10/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '30/10/2019', '0808', '2', '100', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-10-28', 'Kredit', '0', '2');
INSERT INTO `tb_invoice` VALUES ('15', '5', 'AJI/INV/5/05/11/2019', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '05/11/2019', '87778787878787878', '100', 'XL', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2019-11-05', 'Cash', '0', '2');
INSERT INTO `tb_invoice` VALUES ('16', '1', 'AJI/INV/1/25/01/2020', 'JOBS/10/25/01/2020', 'PT. TRIMEGA ', '', '77777888', '1.2', '1', 'Monika', '19/10/2019', 'Ponorogo', 'Jl Raya Nglames Km 5 Tiron Madiun', '2020-01-25', 'Kredit', '10', null);
INSERT INTO `tb_invoice` VALUES ('17', '2', 'AJI/INV/2/25/01/2020', 'JOBS/3/25/01/2020', 'PT. TRIMEGA ', '', '77777888', '2,3', '1', 'KAPAL API', '19/10/2019', 'surabaya', 'Jl Raya Nglames Km 5 Tiron Madiun', '2020-01-25', 'Cash', '10', null);
INSERT INTO `tb_invoice` VALUES ('18', '3', 'AJI/INV/3/30/01/2020', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '', '77777888', '1x2\'', 'XL', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2020-01-30', 'Cash', '10000', null);
INSERT INTO `tb_invoice` VALUES ('19', '4', 'AJI/INV/4/30/01/2020', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '', '77777888', '1x2\'', 'XL', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2020-01-30', 'Cash', '10000', null);
INSERT INTO `tb_invoice` VALUES ('20', '5', 'AJI/INV/4/30/01/2020', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '', '77777888', '1x2\'', 'XL', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2020-01-30', 'Cash', '10000', null);
INSERT INTO `tb_invoice` VALUES ('21', '6', 'AJI/INV/4/30/01/2020', 'JOBS/1/19/10/2019', 'PT. TRIMEGA ', '', '77777888', '1x2\'', 'XL', 'KAPAL API', '19/10/2019', 'JAKARTA', 'BATERAY', '2020-01-30', 'Cash', '10000', null);

-- ----------------------------
-- Table structure for tb_jobs
-- ----------------------------
DROP TABLE IF EXISTS `tb_jobs`;
CREATE TABLE `tb_jobs` (
  `id_jobs` int(11) NOT NULL AUTO_INCREMENT,
  `id_list` int(11) NOT NULL,
  `id_shipment` int(11) NOT NULL,
  `no_job` varchar(100) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `no_bl` varchar(100) NOT NULL,
  `p_o_l` varchar(100) NOT NULL,
  `vessel` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `commodity` varchar(100) NOT NULL,
  `cntr_quantity` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `etd` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `note` text NOT NULL,
  `advance_received_on` varchar(100) NOT NULL,
  `date_create` date NOT NULL,
  `refund_shipper` varchar(100) NOT NULL,
  `voyage` varchar(100) NOT NULL,
  `renmarsk` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jobs`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_jobs
-- ----------------------------
INSERT INTO `tb_jobs` VALUES ('2', '1', '1', 'JOBS/1/19/10/2019', '19-10-2019', '2', '77777888', 'SURABAYA', 'KAPAL API', 'JAKARTA', 'BATERAY', '1x2\'', 'XL', '19/10/2019', '23/10/2019', '', 'sss', '22000000', '2019-10-19', '1000001', 'Hahaha', '10000');
INSERT INTO `tb_jobs` VALUES ('3', '1', '1', 'JOBS/1/14/12/2019', '14-12-2019', '2', '213123123123', 'SURABAYA', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2\'', 'L', '19/10/2019', '14/12/2019', '', 'Di', '20000', '2019-12-14', '10002', 'Huhuhu', '10000');
INSERT INTO `tb_jobs` VALUES ('4', '2', '1', 'JOBS/2/14/12/2019', '14-12-2019', '2', '5676757575', 'SURABAYA', 'KAPAL API', 'surabaya', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2\'', 'M', '19/10/2019', '14/12/2019', '', 'sdadasdas', '20000', '2019-12-14', '1000003', 'Hehehe', '10000');
INSERT INTO `tb_jobs` VALUES ('5', '3', '1', 'JOBS/3/24/12/2019', '24-12-2019', '2', 'Dio', 'Dio', 'Dio', 'Dio', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2', 'xs', '24/12/2019', '27/12/2019', '', 'asdasdasdasd', '20000', '2019-12-24', '1000', 'Hihihihi', '10000');
INSERT INTO `tb_jobs` VALUES ('6', '1', '1', 'JOBS/1/03/01/2020', '03-01-2020', '2', '1111111111111', 'Dio', 'Dio', 'Dio', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2', '1', '19/10/2019', '03/01/2020', '', 'Dio \\Tes', '10000', '2020-01-03', '1000001', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('7', '2', '1', 'JOBS/2/07/01/2020', '07-01-2020', '2', '77777888', 'Dio', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2', '1', '19/10/2019', '07/01/2020', '', 'ggt', '1000', '2020-01-07', '1000001', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('8', '3', '1', 'JOBS/3/25/01/2020', '25-01-2020', '2', '77777888', 'SURABAYA', 'KAPAL API', 'surabaya', 'Jl Raya Nglames Km 5 Tiron Madiun', '2,3', '1', '19/10/2019', '', '', 'asdsadsa', '10000', '2020-01-25', '1000001', 'Diooo', '10');
INSERT INTO `tb_jobs` VALUES ('9', '4', '1', 'JOBS/4/25/01/2020', '25-01-2020', '2', 'dio', 'dio', 'dio', 'dio', 'dio', '1,2', '1', '25/01/2020', '', '', 'asasd', '20000', '2020-01-25', '1000001', 'dio', '10');
INSERT INTO `tb_jobs` VALUES ('10', '5', '1', 'JOBS/5/25/01/2020', '25-01-2020', '2', '77777888', 'SURABAYA', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2\'', '1', '24/12/2019', '', '', 'ssadasd', '20000', '2020-01-25', '1000003', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('11', '6', '1', 'JOBS/6/25/01/2020', '25-01-2020', '2', '213123123123', 'Dio', 'Dio', 'surabaya', 'Jl Raya Nglames Km 5 Tiron Madiun', '1,1', '1', '19/10/2019', '', '', 'asdsadasd', '20000', '2020-01-25', '1000001', 'Diooo', '10');
INSERT INTO `tb_jobs` VALUES ('12', '7', '1', 'JOBS/7/25/01/2020', '25-01-2020', '2', '77777888', 'Dio', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2\'', '2', '24/12/2019', '', '', 'sdadasdasda', '20000', '2020-01-25', '1000001', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('13', '8', '1', 'JOBS/8/25/01/2020', '25-01-2020', '2', '77777888', 'SURABAYA', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '2,4', '1', '19/10/2019', '', '', 'swsdas', '20000', '2020-01-25', '10002', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('14', '9', '1', 'JOBS/9/25/01/2020', '25-01-2020', '2', '213123123123', 'SURABAYA', 'KAPAL API', 'surabaya', 'Jl Raya Nglames Km 5 Tiron Madiun', '1x2\'', '1', '24/12/2019', '', '', 'dsfsdfsdf', '10000', '2020-01-25', '1000001', 'Dio', '10');
INSERT INTO `tb_jobs` VALUES ('15', '10', '1', 'JOBS/10/25/01/2020', '25-01-2020', '2', '77777888', 'Monika', 'Monika', 'Ponorogo', 'Jl Raya Nglames Km 5 Tiron Madiun', '1.2', '1', '19/10/2019', '', '', 'Monika', '20000', '2020-01-25', '1000', 'Monika', '10');
INSERT INTO `tb_jobs` VALUES ('16', '11', '1', 'JOBS/11/27/01/2020', '27-01-2020', '2', 'dasdas', 'SURABAYA', 'KAPAL API', 'JAKARTA', 'Jl Raya Nglames Km 5 Tiron Madiun', '1.2', '1', '19/10/2019', '', '', 'sadasdasd', '1.000', '2020-01-27', '1.000.001', 'Monika', '10');
INSERT INTO `tb_jobs` VALUES ('17', '12', '1', 'JOBS/12/28/01/2020', '28-01-2020', '2', '77777888', 'SURABAYA', 'KAPAL API', 'Ponorogo', 'Jl Raya Nglames Km 5 Tiron Madiun', '1.2', '1', '19/10/2019', '', '', 'scdsdcdcsdcs', '1.000', '2020-01-28', '1.000.003', 'Dio', '10');

-- ----------------------------
-- Table structure for tb_job_extra
-- ----------------------------
DROP TABLE IF EXISTS `tb_job_extra`;
CREATE TABLE `tb_job_extra` (
  `id_job_extra` int(11) NOT NULL AUTO_INCREMENT,
  `no_job` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` varchar(100) NOT NULL,
  PRIMARY KEY (`id_job_extra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_job_extra
-- ----------------------------

-- ----------------------------
-- Table structure for tb_job_opertaional
-- ----------------------------
DROP TABLE IF EXISTS `tb_job_opertaional`;
CREATE TABLE `tb_job_opertaional` (
  `id_job_operational` int(11) NOT NULL AUTO_INCREMENT,
  `no_job` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `debit` varchar(100) NOT NULL,
  `kredit` varchar(100) NOT NULL,
  PRIMARY KEY (`id_job_operational`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_job_opertaional
-- ----------------------------
INSERT INTO `tb_job_opertaional` VALUES ('15', 'JOBS/1/07/10/2019', 'lain-lain', '200.000', '10');
INSERT INTO `tb_job_opertaional` VALUES ('16', 'JOBS/1/12/10/2019', 'Tes Di', '10000', '100');
INSERT INTO `tb_job_opertaional` VALUES ('18', 'JOBS/1/12/10/2019', 'Tes 2', '1000000', '10000');
INSERT INTO `tb_job_opertaional` VALUES ('20', 'JOBS/1/19/10/2019', 'Tes Di', '200.000', '200.000');
INSERT INTO `tb_job_opertaional` VALUES ('21', 'JOBS/1/19/10/2019', 'sssss', '1500000', '50000000');
INSERT INTO `tb_job_opertaional` VALUES ('22', 'JOBS/2/19/10/2019', '', '0', '0');
INSERT INTO `tb_job_opertaional` VALUES ('23', 'JOBS/2/07/01/2020', 'sdcsdc', '20', '10');
INSERT INTO `tb_job_opertaional` VALUES ('24', 'JOBS/1/19/10/2019', '', '200000', '0');
INSERT INTO `tb_job_opertaional` VALUES ('27', 'JOBS/3/20/01/2020', '', '0', '0');
INSERT INTO `tb_job_opertaional` VALUES ('29', 'JOBS/3/25/01/2020', 'Diooooooo', '200', '20');
INSERT INTO `tb_job_opertaional` VALUES ('30', 'JOBS/3/25/01/2020', '', '0', '0');
INSERT INTO `tb_job_opertaional` VALUES ('31', 'JOBS/4/25/01/2020', 'dioSurya', '200', '2');
INSERT INTO `tb_job_opertaional` VALUES ('33', 'JOBS/5/25/01/2020', 'asa', '120', '1');
INSERT INTO `tb_job_opertaional` VALUES ('34', 'JOBS/6/25/01/2020', 'dio', '200', '2');
INSERT INTO `tb_job_opertaional` VALUES ('35', 'JOBS/7/25/01/2020', 'dio Putra', '20', '2');
INSERT INTO `tb_job_opertaional` VALUES ('36', 'JOBS/8/25/01/2020', 'erewrwer', '1', '1');
INSERT INTO `tb_job_opertaional` VALUES ('37', 'JOBS/9/25/01/2020', 'diohaw2', '1.000', '1');
INSERT INTO `tb_job_opertaional` VALUES ('40', 'JOBS/10/25/01/2020', 'Monika 2', '240', '20');
INSERT INTO `tb_job_opertaional` VALUES ('52', 'JOBS/11/27/01/2020', '', '0', '0');
INSERT INTO `tb_job_opertaional` VALUES ('65', 'JOBS/12/27/01/2020', '', '0', '0');
INSERT INTO `tb_job_opertaional` VALUES ('91', 'JOBS/12/28/01/2020', 'asdasdasd', '10.000', '10');
INSERT INTO `tb_job_opertaional` VALUES ('92', 'JOBS/13/28/01/2020', '', '0', '0');

-- ----------------------------
-- Table structure for tb_kas_debet
-- ----------------------------
DROP TABLE IF EXISTS `tb_kas_debet`;
CREATE TABLE `tb_kas_debet` (
  `id_kas_debet` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_transaksi` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_bank` int(11) NOT NULL,
  `nominal` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_kas_debet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kas_debet
-- ----------------------------

-- ----------------------------
-- Table structure for tb_kas_keluar
-- ----------------------------
DROP TABLE IF EXISTS `tb_kas_keluar`;
CREATE TABLE `tb_kas_keluar` (
  `id_kas_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_transaksi` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_bank` int(11) NOT NULL,
  `nominal` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_kas_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kas_keluar
-- ----------------------------
INSERT INTO `tb_kas_keluar` VALUES ('3', '07/10/2019', 'BAYAR INTERNET', '4', '1.000.000', '2019-10-06');
INSERT INTO `tb_kas_keluar` VALUES ('4', '28/10/2019', 'TESTING', '4', '70.000.000', '2019-10-06');
INSERT INTO `tb_kas_keluar` VALUES ('5', '22/10/2019', 'KOKOKOK', '4', '9.500.000', '2019-10-06');
INSERT INTO `tb_kas_keluar` VALUES ('6', '19/10/2019', 'KAS KELUAR', '3', '10.000.000', '2019-10-19');
INSERT INTO `tb_kas_keluar` VALUES ('7', '23/10/2019', 'Tes Dio 10 jt', '3', '10.000.000', '2019-10-28');
INSERT INTO `tb_kas_keluar` VALUES ('8', '29/10/2019', 'Tesss', '3', '5.000.000', '2019-10-29');

-- ----------------------------
-- Table structure for tb_kas_masuk
-- ----------------------------
DROP TABLE IF EXISTS `tb_kas_masuk`;
CREATE TABLE `tb_kas_masuk` (
  `id_kas_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_transaksi` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_bank` int(11) NOT NULL,
  `nominal` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id_kas_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kas_masuk
-- ----------------------------
INSERT INTO `tb_kas_masuk` VALUES ('9', '', 'KAS MASUK', '3', '5.000.000', '0000-00-00');
INSERT INTO `tb_kas_masuk` VALUES ('10', '', 'Tes Dio', '3', '5.000.000', '2019-10-28');
INSERT INTO `tb_kas_masuk` VALUES ('11', '', 'Dio Ini aosakjnskjdn', '3', '10.000', '2019-10-29');
INSERT INTO `tb_kas_masuk` VALUES ('12', '', 'sadasdasdasdasdasd', '3', '10.000', '2019-10-29');
INSERT INTO `tb_kas_masuk` VALUES ('13', '', 'afafafa', '3', '10.000', '2019-10-29');
INSERT INTO `tb_kas_masuk` VALUES ('14', '', 'Dioisodjef', '3', '1.000.000', '2019-10-29');
INSERT INTO `tb_kas_masuk` VALUES ('15', '31/10/2019', 'Disodfsdinfsdnf', '3', '10.000', '2019-10-29');

-- ----------------------------
-- Table structure for tb_piutang
-- ----------------------------
DROP TABLE IF EXISTS `tb_piutang`;
CREATE TABLE `tb_piutang` (
  `id_piutang` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_awal` varchar(100) NOT NULL,
  `tgl_jatuh_tempo` varchar(100) NOT NULL,
  `nominal` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `deskripsi` text NOT NULL,
  `klien` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_Date` date NOT NULL,
  `id_invoice` int(11) NOT NULL,
  PRIMARY KEY (`id_piutang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_piutang
-- ----------------------------
INSERT INTO `tb_piutang` VALUES ('1', '08/12/2019', '11/12/2019', '10000', 'dsffsf', 'dfsdfs', 'Belum Dibayar', '2019-12-08', '0');
INSERT INTO `tb_piutang` VALUES ('2', '', '19/10/2019', '360', '10', 'PT. TRIMEGA ', 'Lunas', '2020-01-25', '16');

-- ----------------------------
-- Table structure for tb_shipment
-- ----------------------------
DROP TABLE IF EXISTS `tb_shipment`;
CREATE TABLE `tb_shipment` (
  `id_shipment` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_shipment` varchar(100) NOT NULL,
  `date_create` date NOT NULL,
  PRIMARY KEY (`id_shipment`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_shipment
-- ----------------------------
INSERT INTO `tb_shipment` VALUES ('1', 'EXPORT LCL', '2019-10-10');
INSERT INTO `tb_shipment` VALUES ('2', 'IMPORT FCL', '2019-10-10');
